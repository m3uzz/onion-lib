<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class Str
{
	
	/**
	 * 
	 * @param string|null $psString
	 * @return string
	 */
	public static function clearStringToUrl (?string $psString) : string
	{
		$lsA = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ/,._!"\'@#$%&*()+=[]{}~ºª;:><|\\';
		$lsB = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                              ';
		$psString = mb_convert_encoding($psString, 'iso8859-1', 'UTF-8');
		$psString = strtr($psString, mb_convert_encoding($lsA, 'iso8859-1', 'UTF-8'), $lsB);
		$psString = trim($psString);
		$psString = preg_replace('/\s\s*/', '-', $psString);
		
		return $psString = strtolower($psString);
	}

	
	/**
	 * 
	 * @param string|null $psString
	 * @return string
	 */
	public static function cyrStrToLower (?string $psString) : string
	{
		// Função não se enquadra no encode ISO-8859-1. Utilizar
		// preferencialmente a função strtolower_iso8859_1
		$lnOffset = 32;
		$laString = [];
		
		for ($lnIdx = 192; $lnIdx < 224; $lnIdx ++)
		{
			$laString[chr($lnIdx)] = chr($lnIdx + $lnOffset);
		}
		
		return strtr($psString, $laString);
	}

	
	/**
	 * 
	 * @param string|null $psString
	 * @return string
	 */
	public static function strToLowerIso8859_1 (?string $psString) : string
	{
		$lnLength = strlen($psString);
		
		while ($lnLength > 0)
		{
			-- $lnLength;
			$lsChar = ord($psString[$lnLength]);
			
			if (($lsChar & 0xC0) == 0xC0)
			{
				// two most significante bits on
				if (($lsChar != 215) and ($lsChar != 223))
				{
					// two chars OK as is to get lowercase set 3. most
					// significante bit if needed:
					$psString[$lnLength] = chr($lsChar | 0x20);
				}
			}
		}
		
		return strtolower($psString);
	}

	
	/**
	 * 
	 * @param string|null $psWord
	 * @return string
	 */
	public static function clearSignals (?string $psWord) : string
	{
		$laReplace = [
			"\"" => "",
			"'" => "",
			"$" => "",
			"\\" => "",
			"/" => "",
			"#" => "",
			"!" => "",
			"%" => "",
			"&" => "",
			"*" => "",
			"(" => "",
			")" => "",
			"-" => "",
			"_" => "",
			"=" => "",
			"`" => "",
			"[" => "",
			"]" => "",
			"{" => "",
			"}" => "",
			"^" => "",
			"~" => "",
			"+" => "",
			"<" => "",
			">" => "",
			"," => "",
			"." => "",
			";" => "",
			":" => "",
			"?" => "",
			"|" => "",
			"@" => ""
		];
		
		return strtr($psWord, $laReplace);
	}

	
	/**
	 * 
	 * @param string|null $psWord
	 * @return string
	 */
	public static function removeAccentuation (?string $psWord) : string
	{
		$laReplace = [
			"à" => "a",
			"â" => "a",
			"á" => "a",
			"ä" => "a",
			"ã" => "a",
			"À" => "A",
			"Â" => "A",
			"Á" => "A",
			"Ä" => "A",
			"Ã" => "A",
			"É" => "E",
			"Ê" => "E",
			"é" => "e",
			"ê" => "e",
			"Ü" => "U",
			"Í" => "I",
			"í" => "i",
			"Ú" => "U",
			"ú" => "u",
			"ü" => "u",
			"Ó" => "O",
			"Ô" => "O",
			"Õ" => "O",
			"ó" => "o",
			"ô" => "o",
			"õ" => "o"
		];
		
		if (!is_null($psWord))
		{
			return strtr($psWord, $laReplace);
		}

		return "";
	}

	
	/**
	 *
	 * @param string|null $lsStr        	
	 * @return string
	 */
	public static function removeISOAccentuation (?string $psStr) : string
	{
		$lsAux = self::strToLowerIso8859_1($psStr);
		
		for ($lnIdx = 0; $lnIdx < strlen($psStr); $lnIdx ++)
		{
			if ($lsAux[$lnIdx] == "á" || $lsAux[$lnIdx] == "à" || $lsAux[$lnIdx] == "ã" || $lsAux[$lnIdx] == "â" || $lsAux[$lnIdx] == "ä")
			{
				$lsAux[$lnIdx] = "a";
			}
			elseif ($lsAux[$lnIdx] == "é" || $lsAux[$lnIdx] == "ê" || $lsAux[$lnIdx] == "è")
			{
				$lsAux[$lnIdx] = "e";
			}
			elseif ($lsAux[$lnIdx] == "í" || $lsAux[$lnIdx] == "ì")
			{
				$lsAux[$lnIdx] = "i";
			}
			elseif ($lsAux[$lnIdx] == "õ" || $lsAux[$lnIdx] == "ó" || $lsAux[$lnIdx] == "ô" || $lsAux[$lnIdx] == "ö" || $lsAux[$lnIdx] == "ò")
			{
				$lsAux[$lnIdx] = "o";
			}
			elseif ($lsAux[$lnIdx] == "ú" || $lsAux[$lnIdx] == "ü" || $lsAux[$lnIdx] == "ù")
			{
				$lsAux[$lnIdx] = "u";
			}
			elseif ($lsAux[$lnIdx] == "ç")
			{
				$lsAux[$lnIdx] = "c";
			}
			elseif ($lsAux[$lnIdx] == "ñ")
			{
				$lsAux[$lnIdx] = "n";
			}
		}
		
		return $lsAux;
	}
	

	/**
	 * Tratamento de entrada de dados para o mysql
	 *
	 * @param string|int|float|bool $pmString  
	 * @return string|array      	
	 */
	public static function escapeString ($pmString)
	{
		$laSearch = [
			"\\",
			"\0",
			"\n",
			"\r",
			"\x1a",
			"'",
			'"'
		];
		$laReplace = [
			"\\\\",
			"\\0",
			"\\n",
			"\\r",
			"\Z",
			"\'",
			'\"'
		];
		
		if (is_object($pmString) || is_array($pmString))
		{
			return $pmString;
		}

		return str_replace($laSearch, $laReplace, (string)$pmString);
	}


	/**
	 * 
	 * @param string|null $psString
	 * @return string
	 */
	public static function unHtmlEntities (?string $psString) : string
	{
		// replace numeric entities
		$psString = preg_replace_callback('~&#x([0-9a-f]+);~i', 'chr(hexdec("\\1"))', $psString);
		$psString = preg_replace_callback('~&#([0-9]+);~', 'chr("\\1")', $psString);
		
		// replace literal entities
		$laTransTbl = get_html_translation_table(HTML_ENTITIES);
		$laTransTbl = array_flip($laTransTbl);
		
		return strtr($psString, $laTransTbl);
	}
	
	
	/**
	 *
	 * @param string|null $psStr
	 * @param int $pnLen
	 * @return string
	 */
	public static function tagGenerator (?string $psStr, int $pnLen = 10) : string
	{
		$laReplace = [
			"\"" => "",
			"'" => "",
			"\$" => "",
			"\\" => "",
			"/" => "",
			"#" => "",
			"!" => "",
			"%" => "",
			"&" => "",
			"*" => "",
			"(" => "",
			")" => "",
			"_" => "",
			"=" => "",
			"`" => "",
			"~" => "",
			"[" => "",
			"]" => "",
			"{" => "",
			"}" => "",
			"^" => "",
			"<" => "",
			">" => "",
			"," => "",
			"." => "",
			";" => "",
			":" => "",
			"?" => "",
			"|" => "",
			"@" => "",
			" +" => " ",
			" -" => " "
		];
	
		//$psStr = self::unHtmlEntities($psStr);
		$psStr = strtr($psStr, $laReplace);
		$psStr = substr($psStr, 0, $pnLen);
		$psStr = self::removeAccentuation($psStr);
		$psStr = trim($psStr);
	
		$psStr = preg_replace([
			"/[\s]+/"
		], [
			" "
		], $psStr);
	
		$psStr = strtoupper(str_replace(' ', '-', $psStr));
	
		return $psStr;
	}
	
	
	/**
	 *
	 * @param string|null $psString
	 * @return string
	 */
	public static function lcfirst (?string $psString) : string
	{
		$lsString = self::slugfy($psString);
		$laArray = explode("-", $lsString);
	
		if (is_array($laArray))
		{
			$lsString = $laArray[0];
			unset($laArray[0]);
				
			foreach($laArray as $lsValue)
			{
				$lsString .= ucfirst($lsValue);
			}
		}
	
		return $lsString;
	}
	
	
	/**
	 * 
	 * @param string|null $psText
	 * @return string
	 */
	public static function slugfy (?string $psText) : string
	{
		$psText = self::removeAccentuation($psText);
		$psText = preg_replace("/[^A-Za-z]/", "", $psText);
		$psText = preg_replace("/([A-Z])/", " $1", $psText);
		$psText = preg_replace("/\s\s/", " ", $psText);
		$psText = preg_replace("/\s/", "-", trim($psText));
		
		return strtolower($psText);
	}
	
	
	/**
	 *
	 * @param string|null $psText
	 * @return string
	 */
	public static function slugToCamelCase (?string $psString) : string
	{
	    $lsString = '';
	    $laArray = explode("-", $psString);
	    
	    if (is_array($laArray))
	    {
	        foreach($laArray as $lsValue)
	        {
	            $lsString .= ucfirst($lsValue);
	        }
	    }
	    
	    return $lsString;
	}
	
	
	/**
	 *
	 * @param string|null $psVar
	 * @param string|null $psValue
	 * @param string $psSeparator
	 * @return string
	 */
	public static function appendStr (?string $psVar, ?string $psValue, string $psSeparator = " ") : string
	{
		$psVar = trim($psVar);
		
		if (!empty($psVar))
		{
			$psVar .= $psSeparator;
		}
		
		$psVar .= $psValue;
		
		return $psVar;
	}
	
	
	/**
	 *
	 * @param string|null $psVar
	 * @param string|null $psValue
	 * @param string $psSeparator
	 * @return string
	 */
	public static function prependStr (?string $psVar, ?string $psValue, string $psSeparator = " ") : string
	{
		$psVar = trim($psVar);
		
		if (!empty($psVar))
		{
			$psValue .= $psSeparator;
		}
		
		$psValue .= $psVar;
		
		return $psValue;
	}
	
	
	/**
	 *
	 * @param string|null $psVar
	 * @param string|null $psValue
	 * @param string $psSeparator
	 * @return string
	 */
	public static function removeStr (?string $psVar, ?string $psValue, string $psSeparator = " ") : string
	{
		$laVar = explode($psSeparator, $psVar);
		
		foreach ($laVar as $lnKey => $lsValue)
		{
			if ($lsValue == $psValue)
			{
				unset($laVar[$lnKey]);
			}
		}
		
		$psVar = implode($psSeparator, $laVar);
		
		return $psVar;
	}
}