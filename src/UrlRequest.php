<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;

defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class UrlRequest
{
	/**
	 * 
	 * @var UrlRequest
	 */
	private static $oInstance;

	/**
	 * Link para o servidor
	 * @var string
	 */
	protected $sServer = null;
	
	/**
	 * HTTP Version, informa o tipo de requisição http
	 * @var string
	 */
	protected $sHttpVersion = "HTTP/1.0";
	
	/**
	 * Token de autenticação para acesso ao server
	 * @var string
	 */
	protected $sToken = null;

	/**
	 * UserAgent, identificação do browser
	 * @var string
	 */
	protected $sUserAgent = null;

	/**
	 * UserAgent, identificação do browser
	 * @var string
	 */
	protected $sUUID = null;
	
	/**
	 * UserAgent, identificação do browser
	 * @var string
	 */
	protected $sAccept = null;
	
	/**
	 * UserEncoding, identificação do browser
	 * @var string
	 */
	protected $sAcceptEncoding = null;
	
	/**
	 * UserLanguage, identificação do browser
	 * @var string
	 */
	protected $sAcceptLanguage = null;
	
	/**
	 * Referer, identificação de onde está vindo a chamada
	 * @var string
	 */
	protected $sReferer = null;
	
	/**
	 * Charset
	 * @var string
	 */
	protected $sCharset = "utf-8";
		
	/**
	 * Metodo de requisição ao server, `GET` ou `POST`
	 * @var string
	 */
	protected $sMethod = "GET";

	/**
	 * Manter conexão ativa ou encerrar ao final da trasação
	 * @var string
	 */
	protected $sConnection = "Close"; //keep-alive
		
	/**
	 * Porta padrão de requisição
	 * @var int
	 */
	protected $nPort = 80;
	
	/**
	 * Tempo de espera para resposta de uma requisição
	 * @var int
	 */
	protected $nTimeOut = 15;
	
	/**
	 * Verificar se há uma conexão ativa com o server
	 * @var bool
	 */
	protected $bCheckOnly = false;

	/**
	 * Tamanho do requisição de dados para o servidor
	 * @var int
	 */
	protected $nGetLength = 1024;
	
	/**
	 * Metodo de callback para a requisição
	 * @var callable
	 */
	protected $cCallBack = null;
	
	/**
	 * Se deve fechar a conexão ao final da chamada
	 * @var bool
	 */
	protected $bClose = true;

	/**
	 * Conteúdo do cabeçalho da última requisição
	 * @var array
	 */
	protected $aResponseHeader = null;

	/**
	 * http header de retorno
	 * @var string
	 */
	protected $sHttpHeader = "text/plain";

	/**
	 * Array de parametros a ser passado para o service
	 * @var array
	 */
	protected $aParams = null;

	/**
	 * parametros codificados em json mode
	 * @var string
	 */
	protected $sJson = null;
		
	/**
	 * Dados de arquivo
	 * @var string
	 */
	protected $sFile = null;
	
	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance () : UrlRequest
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self;
		}
		
        return self::$oInstance;
	}
	
	
	/**
	 * 
	 */
	private function __construct()
	{
		if(isset($_SERVER['HTTP_USER_AGENT'])){
			$this->sUserAgent = $_SERVER['HTTP_USER_AGENT'];
		}
		else
		{
			$this->sUserAgent = "Onion_WebService";
		}
		
		if(isset($_SERVER['HTTP_ACCEPT'])){
			$this->sAccept = $_SERVER['HTTP_ACCEPT']; 
		}
		
		if(isset($_SERVER['UUID'])){
			$this->sUUID = $_SERVER['UUID'];
		}
		
		if(isset($_SERVER['HTTP_ACCEPT_ENCODING'])){
			$this->sAcceptEncoding = $_SERVER['HTTP_ACCEPT_ENCODING'];
		}
		
		if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
			$this->sAcceptLanguage = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		}
	}

	
	/**
	 * __set
	 * 
	 * @param string $psVar
	 * @param string $psValue
	 */
	public function __set (string $psVar, string $psValue)
	{
		return $this->set($psVar, $psValue);
	}
	
	
	/**
	 * set
	 * 
	 * @param string $psVar
	 * @param string $psValue
	 */
	public function set (string $psVar, string $psValue) : UrlRequest
	{
		if(property_exists($this, $psVar))
		{
			$this->$psVar = $psValue;
		}
		
		return $this;
	}

	
	/**
	 * __get
	 * 
	 * @param string $psVar
	 * @return mixed
	 */
	public function __get (string $psVar)
	{		
		return $this->get($psVar);
	}

	
	/**
	 * get
	 *
	 * @param string $psVar
	 * @return mixed
	 */
	public function get (string $psVar)
	{
		if(property_exists($this, $psVar))
		{
			return $this->$psVar;
		}
	
		return false;
	}
	
	
	/**
	 * 
	 * @return void
	 */
	public function __print () : void
	{
		var_dump(get_object_vars($this));	
	}
	
	
	/**
	 * Metodo de requisição ao servidor ou local
	 * 
	 * @param string $psLink link para conexão e requisição ao servidor
	 * @return string|bool
	 * @throws \Exception
	 */
	public function urlrequest (string $psLink)
	{  
		$lsStream = null;
		$laLink = parse_url($psLink); //interpreta o link e retorna seus componentes
	
		if(isset($laLink['port']))
		{
			//Se a porta não estiver setada no link é considerada a porta padrão
			$this->nPort = $laLink['port'];
		}
		
		//Debug::debug($laLink);
		
		//Verificando se o host está setado
		if(isset($laLink['host']))
		{
			//Tentando abrir uma conexão com o servidor até o prazo limit (_nTimeOut), retornando um token de conexão
			//Caso ocorra algum erro, é retornado false e o número ($lnErrNo) e mensagem ($lsErrStr) do erro
			$lrConnection = @fsockopen($laLink['host'], $this->nPort, $lnErrNo, $lsErrStr, $this->nTimeOut);
		
			if(!$lrConnection)
			{
				//Se a conexão não tiver sido estabelecida é gerado um log de erro
				throw new \Exception("Failed to connect on server ({$psLink})! {$lsErrStr}", $lnErrNo);
				//Debug::debug(["Failed to connect on server (".$psLink.")", $lsErrStr]);
			}
			else
			{
				$lsPost = "";
				$lsGet = "";
				$lsQuery = "";
				$lsPath = "";
				
				if(isset($laLink['query']))
				{
					$lsPost = "Content-length: " . strlen($laLink['query']) . "\r\n";
					$lsGet = "?" . $laLink['query'];
					$lsQuery = $laLink['query'];
				}

				if(isset($laLink['path']))
				{
					$lsPath = $laLink['path'];
				}
				
				//Caso a conexão tenha sido estabelecida verifica o metodo de requisição
				if($this->sMethod === "POST")
				{
					$lsRequest  = "POST " . $lsPath . " $this->sHttpVersion\r\n";
					$lsRequest .= "Host: " . $laLink['host'] . "\r\n";
					$lsRequest .= "User-Agent: " . $this->sUserAgent . "\r\n";
					
					if(!empty($this->sUUID))
						$lsRequest .= "UUID: " . $this->sUUID . "\r\n";
					
					if(!empty($this->sAccept))
						$lsRequest .= "Accept: " . $this->sAccept . "\r\n";
					
					if(!empty($this->sAcceptLanguage))
						$lsRequest .= "Accept-Language: " . $this->sAcceptLanguage . "\r\n";
					
					if(!empty($this->sAcceptEncoding))
						$lsRequest .= "Accept-Encoding: " . $this->sAcceptEncoding . "\r\n";
					
					if(!empty($this->sReferer))
						$lsRequest .= "Referer: " . $this->sReferer . "\r\n";
					
					if(!empty($this->sToken))
						$lsRequest .= "Token: " . $this->sToken . "\r\n";
					
					if(!empty($this->sJson))					
						$lsRequest .= "Json: " . $this->sJson . "\r\n";
					
					$lsRequest .= "Content-type: application/x-www-form-urlencoded; charset=" . $this->sCharset . "\r\n";
					$lsRequest .= $lsPost;
					$lsRequest .= "Connection: Close\r\n\r\n";
					$lsRequest .= $lsQuery;
				}
				elseif($this->sMethod === "JSON")
				{
					$lsRequest  = "POST " . $lsPath . " $this->sHttpVersion\r\n";
					$lsRequest .= "Host: " . $laLink['host'] . "\r\n";
					$lsRequest .= "User-Agent: " . $this->sUserAgent . "\r\n";
					
					if(!empty($this->sUUID))
						$lsRequest .= "UUID: " . $this->sUUID . "\r\n";
					
					if(!empty($this->sAccept))
						$lsRequest .= "Accept: " . $this->sAccept . "\r\n";
					
					if(!empty($this->sAcceptLanguage))
						$lsRequest .= "Accept-Language: " . $this->sAcceptLanguage . "\r\n";
					
					if(!empty($this->sAcceptEncoding))
						$lsRequest .= "Accept-Encoding: " . $this->sAcceptEncoding . "\r\n";
					
					if(!empty($this->sReferer))
						$lsRequest .= "Referer: " . $this->sReferer . "\r\n";
					
					if(!empty($this->sToken))
						$lsRequest .= "Token: " . $this->sToken . "\r\n";
					
					$lsRequest .= "Data-type: json\r\n";
					$lsRequest .= "Content-type: application/x-www-form-urlencoded; charset=" . $this->sCharset . "\r\n";
					$lsRequest .= "Content-length: " . strlen($this->sJson) . "\r\n";
					$lsRequest .= "Connection: " . $this->sConnection . "\r\n\r\n";
					$lsRequest .= "data=" . $this->sJson . "\r\n";					
				}
				elseif($this->sMethod === "PUT")
				{
					$lsRequest  = "PUT " . $lsPath . " $this->sHttpVersion\r\n";
					$lsRequest .= "Host: " . $laLink['host'] . "\r\n";
					$lsRequest .= "User-Agent: " . $this->sUserAgent . "\r\n";
					
					if(!empty($this->sUUID))
						$lsRequest .= "UUID: " . $this->sUUID . "\r\n";
					
					if(!empty($this->sAccept))
						$lsRequest .= "Accept: " . $this->sAccept . "\r\n";
					
					if(!empty($this->sAcceptLanguage))
						$lsRequest .= "Accept-Language: " . $this->sAcceptLanguage . "\r\n";
					
					if(!empty($this->sAcceptEncoding))
						$lsRequest .= "Accept-Encoding: " . $this->sAcceptEncoding . "\r\n";
					
					if(!empty($this->sReferer))
						$lsRequest .= "Referer: " . $this->sReferer . "\r\n";
					
					if(!empty($this->sToken))
						$lsRequest .= "Token: " . $this->sToken . "\r\n";
					
					$lsRequest .= "Content-type: application/x-www-form-urlencoded; charset=" . $this->sCharset . "\r\n";
					$lsRequest .= "Content-length: " . strlen($this->sFile) . "\r\n";
					$lsRequest .= "Connection: " . $this->sConnection . "\r\n\r\n";
					$lsRequest .= $this->sFile;
				}
				else
				{
					$lsRequest = "GET " . $lsPath . $lsGet . " $this->sHttpVersion\r\n";
					$lsRequest .= "Host: " . $laLink['host'] . "\r\n";
					$lsRequest .= "User-Agent: " . $this->sUserAgent . "\r\n";
					
					if(!empty($this->sUUID))
						$lsRequest .= "UUID: " . $this->sUUID . "\r\n";
					
					if(!empty($this->sAccept))
						$lsRequest .= "Accept: " . $this->sAccept . "\r\n";
					
					if(!empty($this->sAcceptLanguage))
						$lsRequest .= "Accept-Language: " . $this->sAcceptLanguage . "\r\n";
					
					if(!empty($this->sAcceptEncoding))
						$lsRequest .= "Accept-Encoding: " . $this->sAcceptEncoding . "\r\n";
					
					if(!empty($this->sReferer))
						$lsRequest .= "Referer: " . $this->sReferer . "\r\n";
					
					if(!empty($this->sToken))
						$lsRequest .= "Token: " . $this->sToken . "\r\n";
					
					$lsRequest .= "Connection: " . $this->sConnection . "\r\n\r\n";
				}

				//Debug::debug($lsRequest);
				
				$lbErro = false;
				$lbHeaderSection = true;
				$this->aResponseHeader = [];
				
				//Enviando requisição para o servidor
				if(!fwrite($lrConnection, $lsRequest))
				{
					//Se a requisição falhar retorna um erro
					throw new \Exception("Failed to write on server ({$psLink})!");
					//Debug::debug(["Failed to write on server (".$psLink.")"]);
					$lbErro = true;
				}

				$lsHeaderStatusLine = '';
				
				//Lendo a requisição do servidor em pacotes
				while(!feof($lrConnection) && !$lbErro)
				{
					if($lbHeaderSection)
					{
						//Lendo o cabeçado da requisição
						$lsHeader = fgets($lrConnection, $this->nGetLength);
		
						if($lsHeader !== "\r\n")
						{
							
							//Enquanto não chegar ao fim do cabeçalho, atribuir cada linha a um array
							$laHeaderLine = explode(': ', trim($lsHeader));
							$lsValue = isset($laHeaderLine[1])?$laHeaderLine[1]:$laHeaderLine[0];
							$lsVar = isset($laHeaderLine[1])?$laHeaderLine[0]:'';

							if (!empty($lsValue) && empty($lsVar))
							{
								$lsVar = 'Status' . $lsHeaderStatusLine;
								$lsHeaderStatusLine++;
							}
							
							if (isset($this->aResponseHeader[$lsVar]))
							{
								if (!is_array($this->aResponseHeader[$lsVar]))
								{
									$lsVal = $this->aResponseHeader[$lsVar];
									
									$this->aResponseHeader[$lsVar] = [];
									$this->aResponseHeader[$lsVar][] = $lsVal;
								}

								$this->aResponseHeader[$lsVar][] = $lsValue;
							}
							else
							{
								$this->aResponseHeader[$lsVar] = $lsValue;
							}
						}
						else
						{
							//Se a leitura do cabeçalho chegar ao fim, fechar a seção para a leitura do conteúdo
							$lbHeaderSection = false;
						}
		
						if(!$lbHeaderSection)
						{
							//Se o cabeçalho da requisição tiver acabado, verifica o status da requisição
							if(!preg_match("/HTTP.*200 OK/", $this->aResponseHeader['Status'][0]))
							{
								$this->aResponseHeader['Error'] = true;
								//Se a requisição não encontar o arquivo e retornar um código diferente de 200, é gerado um log de erro
								throw new \Exception($this->aResponseHeader['Status'][1] . " (".$psLink.")");
								//Debug::debug([$this->aResponseHeader['Status'][1]." (".$psLink.")"]);
								$lbErro = true;
							}
		
							if($this->bCheckOnly)
							{
								//Se _bCheckOnly estiver setada como true, verifica somente o cabeçalho
								return !$lbErro;
							}
						}
					}
					else
					{
						if(!is_null($this->cCallBack))
						{
							$lsMethod = $this->cCallBack;
							//Lendo o arquivo do servidor e tratando linha a linha através da função callback
							$lsMethod(fgets($lrConnection, $this->nGetLength));
						}
						else
						{
							//Lendo o arquivo do servidor e armazenando em uma string 
							$lsStream .= fgets($lrConnection, $this->nGetLength);
						}
					}
				}
		
				if($this->bClose)
				{
					//Se a conexão deve ser fechada ao final da requisição e leitura
					fclose($lrConnection);
				}

				if (!$lbErro && isset($this->aResponseHeader['Content-Encoding']) && $this->aResponseHeader['Content-Encoding'] == 'gzip')
				{
					$lsStream = gzdecode($lsStream);
				}
				
				//Retornando o conteúdo do arquivo
				return $lsStream;
			}
		}
	
		//Retorno falso caso não tenha estabelicido a conexão com o servidor
		return false;
	}
	
	
	/**
	 * Metodo de requisição de serviço no servidor
	 * 
	 * @param string|null $psService caminho do arquivo no servidor (modulo/controller ou service)
	 * @param string|null $psAction metodo ou action a ser executada pelo service
	 * @return string|bool
	 */
	public function callService (?string $psService = null, ?string $psAction = null)
	{
		$lsQuery = "";

		if(is_array($this->aParams))
		{
			$lsSeparate = "";

			//Transformando array de parametros em string
			foreach($this->aParams as $lsVar => $lsValue)
			{
				$lsQuery .= $lsSeparate . urlencode($lsVar) . "=" . urlencode($lsValue);
				$lsSeparate = "&";
			}
		}
		
		$psService = isset($psService) ? "/" . $psService : "";
		$psAction = isset($psAction) ? "/" . $psAction : "";
		$lsQuery = !empty($lsQuery) ? "/?" . $lsQuery : "";
		
		$lsLink = $this->sServer . $psService . $psAction . $lsQuery;

		return $this->urlrequest($lsLink);		
	} 
}