<?php
/**
 * This file is part of Onion Lib
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use OnionLib\Debug;
use OnionLib\System;


defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class Event
{

	/**
	 * 
	 * @param mixed $pmLine
	 * @param string $psFileName
	 * @param string $psSeparate
	 * @param bool $pbForceLog
	 */
	public static function log ($pmLine, string $psFileName = 'event', string $psSeparate = "\t", bool $pbForceLog = false) : void
	{
		$lsFileName = defined('ONIONLOGPATH') ? constant('ONIONLOGPATH') : '';
		$lbEnable = defined('ONIONLOGENABLE') ? constant('ONIONLOGENABLE') : true;
		
		if (!empty($psFileName))
		{
			$lsFileName .= DS . $psFileName . '_' . date('Y-m') . ".log";
		}
		else
		{
			$lsFileName .= DS . 'event_'  . date('Y-m') . ".log";
		}
		
		if ($lbEnable || $pbForceLog)
		{		
			$lsLine = $pmLine;
		
			if (is_array($pmLine))
			{
				$lsLine = implode ($psSeparate, $pmLine);
			}
		
			$lsLog = date('Y-m-d H:i:s', time()) . $psSeparate . $lsLine . "\n";
		
			if (file_exists($lsFileName))
			{
				System::saveFile($lsFileName, $lsLog, 'APPEND');
			}
			else
			{
				System::saveFile($lsFileName, $lsLog);
			}
		}
		else 
		{
			Debug::debug($pmLine);
		}
	}
}