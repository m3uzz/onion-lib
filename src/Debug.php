<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use OnionLib\System;
//use FirePHP;

defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class Debug
{
	/**
	 * @var array
	 */
	protected static $aBuffer = [];

	/**
	 * @var array
	 */
	protected static $aTimer = [];

	/**
	 * @var int
	 */
	protected static $nTrace = 1;

	/**
	 * @var bool
	 */
	public static $bDebug = false;
	
	/**
	 * @var string
	 */
	public static $sOutput = null;


	/**
	 * 
	 * @param string|null $psOutput força o tipo de saída de impressão (`DISPLAY`|`JSON`|`COMMENT`|`FIREBUG`|`BUFFER`|`FILE`)
	 */
	public static function on (?string $psOutput = null)
	{
		if (!is_null($psOutput))
		{
			self::$sOutput = $psOutput;
		}

		self::$bDebug = true;
	}


	/**
	 * 
	 */
	public static function off ()
	{
		self::$bDebug = false;
	}


	/**
	 *
	 * @param int|string|array|object $pmVar Valor a ser impresso
	 * @param bool $pbForceDebug Se true força a impressão mesmo que no config o debug esteja desabilitado
	 * @param string|null $psOutput força o tipo de saída de impressão (`DISPLAY`|`JSON`|`COMMENT`|`FIREBUG`|`BUFFER`|`FILE`)
	 */
	public static function debug ($pmVar, bool $pbForceDebug = false, ?string $psOutput = null) : void
	{
		$lbDebug = defined('DEBUG') ? constant('DEBUG') : self::$bDebug;

		if ($lbDebug || self::$bDebug || $pbForceDebug) 
		{
			$lsOutput = defined('DEBUGOUTPUT') ? constant('DEBUGOUTPUT') : 'DISPLAY';

			if (self::$sOutput != null) $lsOutput = self::$sOutput;

			if ($psOutput != null) $lsOutput = $psOutput;

			$laTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, self::$nTrace);

			$lsTrace = date('Y-m-d H:i:s, ') . "file: " . $laTrace[self::$nTrace - 1]['file'] . ", line: " . $laTrace[self::$nTrace - 1]['line'];

			self::$nTrace = 1;

			switch ($lsOutput) 
			{
				case "COMMENT":
					echo "<!-- Onion Debug: ";
					echo "{$lsTrace}\n";
					echo self::displayDebug($pmVar);
					echo "\n-->\n\n";
					break;
				case "BUFFER":
					$laContent['trace'] = $lsTrace;
					$laContent["message"] = is_object($pmVar) ? (array)$pmVar : $pmVar;	
					self::$aBuffer[] = $laContent;
					break;
				case "FILE":
					$lsContent = "Onion Debug: {$lsTrace}\n";
					$lsContent .= self::displayDebug($pmVar);
					$lsContent .= "\n\n";
					$lsLogPath = defined('ONIONLOGPATH') ? constant('ONIONLOGPATH') : '';
					System::saveFile($lsLogPath . DS . "debug_" . date('Y-m') . ".log", $lsContent, "APPEND");
					break;
				/*case "FIREBUG":
					$loFirePHP = FirePHP::getInstance(true);
					$loFirePHP->setOption('includeLineNumbers', false);
					$loFirePHP->fb("Onion Debug: {$lsTrace}");
					$loFirePHP->fb(self::displayDebug($pmVar));
					break;*/
				case "JSON":
					$laContent["Onion Debug"] = $lsTrace;
					$laContent["Message"] = is_object($pmVar) ? (array)$pmVar : $pmVar;				
					$lsContent = json_encode($laContent, JSON_PRETTY_PRINT);
					echo "{$lsContent}\n\n";
					break;					
				default:
					if (PHP_SAPI == "cli") 
					{
						echo "\nOnion Debug: ";
						echo "{$lsTrace}\n";
						echo self::displayDebug($pmVar);
						echo "\n\n";
					} 
					else 
					{
						echo '<pre class="onion-debug" style="margin:100px; padding: 20px; background:#ccc;"><code><fieldset><legend>Onion Debug:</legend>';
						echo "<br/>{$lsTrace}<br/>";
						echo self::displayDebug($pmVar);
						echo '</fieldset></code></pre>';
					}
			}
		}
	}


	/**
	 *
	 * @param int|string|array|object $pmVar
	 * @param bool $pbForceDebug Se true força a impressão mesmo que no config o debug esteja desabilitado
	 * @param string|null $psOutput força o tipo de saída de impressão (`DISPLAY`|`COMMENT`|`FIREBUG`|`BUFFER`|`FILE`)
	 */
	public static function debugD ($pmVar, bool $pbForceDebug = false, ?string $psOutput = null) : void
	{
		self::$nTrace = 2;
		self::debug($pmVar, $pbForceDebug, $psOutput);

		$lbDebug = defined('DEBUG') ? constant('DEBUG') : true;

		if ($lbDebug || self::$bDebug || $pbForceDebug) 
		{
			exit(200);
		}
	}


	/**
	 *
	 * @param int|string|array|object $pmVar        	
	 */
	public static function display ($pmVar) : void
	{
		self::$nTrace = 2;

		self::debug($pmVar, true, "DISPLAY");
	}


	/**
	 *
	 * @param int|string|array|object $pmVar        	
	 */
	public static function displayD ($pmVar) : void
	{
		self::$nTrace = 2;

		self::debug($pmVar, true, "DISPLAY");
		exit(200);
	}


	/**
	 *
	 * @param int|string|array|object $pmVar        	
	 */
	public static function displayDebug ($pmVar) : string
	{
		if (is_object($pmVar)) 
		{
			return print_r((array)$pmVar, true);
		} 
		elseif (is_array($pmVar)) 
		{
			return print_r($pmVar, true);
		} 

		return (string)$pmVar;
	}


	/**
	 * 
	 */
	public static function showDebug () : void
	{
		$lbDebug = defined('DEBUG') ? constant('DEBUG') : true;
		$lsDebugOutput = defined('DEBUGOUTPUT') ? constant('DEBUGOUTPUT') : "BUFFER";

		if (($lbDebug || self::$bDebug) && ($lsDebugOutput == "BUFFER" || self::$sOutput == "BUFFER")) 
		{
			if (is_array(self::$aBuffer)) 
			{
				echo '<pre class="onion-debug" style="margin:100px; padding: 20px; background:#ccc;"><code><fieldset><legend>Onion Debug:</legend>';

				foreach (self::$aBuffer as $laItem) 
				{
					echo "<br/>{$laItem['trace']}<br/>";
					echo self::displayDebug($laItem['message']);
					echo "<hr style=\"border:0; border-width:1px 0 0 0; border-style:solid none none none; border-color:#666;\"/><br/>";
				}

				echo '</fieldset></code></pre>';
			}
		}
	}


	/**
	 * 
	 * @param string $psMethod
	 * @param bool $pbUnique
	 * @param bool $pbForceShowTime
	 * @param float|null $pnTime
	 */
	public static function debugTimeStart (string $psMethod, bool $pbUnique = false, bool $pbForceShowTime = false, ?float $pnTime = null) : void
	{
		self::debugTimer($psMethod, 'START', $pbUnique, $pbForceShowTime, $pnTime);
	}


	/**
	 * 
	 * @param string $psMethod
	 * @param bool $pbUnique
	 * @param bool $pbForceShowTime
	 * @param float|null $pnTime
	 */
	public static function debugTimeEnd (string $psMethod, bool $pbUnique = false, bool $pbForceShowTime = false, ?float $pnTime = null) : void
	{
		self::debugTimer($psMethod, 'END', $pbUnique, $pbForceShowTime, $pnTime);
	}


	/**
	 * 
	 * @param string $psMethod
	 * @param string $psType
	 * @param bool $pbUnique
	 * @param bool $pbForceShowTime
	 * @param float|null $pnTime
	 */
	public static function debugTimer (string $psMethod, string $psType, bool $pbUnique = false, bool $pbForceShowTime = false, ?float $pnTime = null) : void
	{
		$lbTimeShow = defined('TIMESHOW') ? constant('TIMESHOW') : true;

		if ($lbTimeShow || $pbForceShowTime) 
		{
			if ($pnTime !== null) 
			{
				$lnTime = $pnTime;
			} 
			else 
			{
				$laTime = explode(" ", microtime());
				$lnTime = $laTime[1] + $laTime[0];
			}

			$lsMsg = $psMethod . " [" . $psType . "] " . $lnTime;

			if ($pbUnique) 
			{
				if ($psType == 'START' && !isset(self::$aTimer[$psMethod]['START'][0])) 
				{
					self::$aTimer[$psMethod]['START'][0] = $lnTime;
					self::debug("{$psMethod} {$psType}: {$lnTime}", true);
				} 
				elseif ($psType == 'END') 
				{
					self::$aTimer[$psMethod]['END'][0] = $lnTime;
					echo ".";
				}
			} 
			else 
			{
				self::$aTimer[$psMethod][$psType][] = $lnTime;
				self::debug("{$psMethod} {$psType}: {$lnTime}", true);
			}

			if ($psType == 'END') 
			{
				if ($pbUnique) 
				{
					self::$aTimer[$psMethod]['TIME'][0] = self::$aTimer[$psMethod]['END'][0] - self::$aTimer[$psMethod]['START'][0];
				} 
				else 
				{
					self::$aTimer[$psMethod]['TIME'][] = 1;
					$lnI = count(self::$aTimer[$psMethod]['TIME']) - 1;
					self::$aTimer[$psMethod]['TIME'][$lnI] = self::$aTimer[$psMethod]['END'][$lnI] - self::$aTimer[$psMethod]['START'][$lnI];
					self::debug("{$psMethod} TIME: " . self::$aTimer[$psMethod]['TIME'][$lnI], true);
				}
			}
		}
	}


	/**
	 * 
	 * @param string $psMethod
	 * @param string|null $psType
	 * @param bool $pbForceShowTime
	 */
	public static function showTime (string $psMethod, ?string $psType = null, bool $pbForceShowTime = false) : void
	{
		$lbTimeShow = defined('TIMESHOW') ? constant('TIMESHOW') : true;

		if ($lbTimeShow || $pbForceShowTime) 
		{
			if ($psType === null) 
			{
				if (isset(self::$aTimer[$psMethod])) 
				{
					self::debug("{$psMethod}:", true);
					self::debug(self::$aTimer[$psMethod], true);
				}
			} 
			else 
			{
				if (isset(self::$aTimer[$psMethod][$psType])) 
				{
					self::debug("{$psMethod} {$psType}:", true);
					self::debug(self::$aTimer[$psMethod][$psType], true);
				}
			}
		}
	}


	/**
	 * 
	 */
	public static function displayTime () : void
	{
		self::$nTrace = 2;
		$lbTimeShow = defined('TIMESHOW') ? constant('TIMESHOW') : true;

		self::debug(self::$aTimer, $lbTimeShow);
	}
}