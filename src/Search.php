<?PHP
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use OnionLib\Str;


class Search
{

	/**
	 * @var array|string
	 */
	protected $mSearchFields;

	/**
	 * @var int
	 */
	protected $nMaxLenQuery = 100;

	/**
	 * @var bool
	 */
	protected $bUseStopWords = false;

	/**
	 * @var string
	 */
	protected $sOriginalQuery;

	/**
	 * @var string
	 */
	protected $sQuery;

	/**
	 * @var string
	 */
	protected $sWhere = "";

	/**
	 * @var string
	 */
	protected $sSelect = "";

	/**
	 * @var string
	 */
	protected $sSearchType;

	/**
	 * @var string
	 */
	protected $sExpression;

	/**
	 * @var array
	 */
	protected $aBoolWords;

	/**
	 * @var array
	 */
	protected $aWithWords;

	/**
	 * @var array
	 */
	protected $aWithoutWords;

	/**
	 * @var array
	 */
	protected $aOrWords;

	/**
	 * @var array
	 */
	protected $aAndWords;

	/**
	 * @var int
	 */
	protected $nError = 0;
	
	/**
	 * @var object
	 */
	protected $oDb;
	

	/**
	 * 
	 * @param string $psVar
	 * @param mixed $pmValue
	 */
	public function set (string $psVar, $pmValue) : void
	{
		if (property_exists($this, $psVar))
		{
			$this->$psVar = $pmValue;
		}
	}


	/**
	 * 
	 * @param string $psVar
	 * @return mixed
	 */
	public function get (string $psVar)
	{
		if (property_exists($this, $psVar))
		{
			return $this->$psVar;
		}
	}

	
	/**
	 * 
	 * @param string|array $pmValue
	 * @return string|array
	 */
	public function setSearchFields ($pmValue)
	{
		return $this->mSearchFields = $pmValue;
	}


	/**
	 * 
	 * @param int $pnValue
	 * @return int
	 */
	public function setMaxLenQuery (int $pnValue) : int
	{
		return $this->nMaxLenQuery = $pnValue;
	}


	/**
	 * 
	 * @param bool $pbValue
	 * @return bool
	 */
	public function setUseStopWords (bool $pbValue) : bool
	{
		return $this->bUseStopWords = $pbValue;
	}


	/**
	 * 
	 * @return string
	 */
	public function getWhere () : string
	{
		return $this->sWhere;
	}


	/**
	 * 
	 * @return string
	 */
	public function getSelectScore () : string
	{
		return $this->sSelect;
	}


	/**
	 * TODO: refazer
	 */
	protected function stopWords () : void
	{
		if ($this->sSearchType != "boolean")
		{
			return;
		}
		
		$laQuery = $this->aBoolWords;
		
		if ($this->bUseStopWords && is_array($laQuery))
		{
			$this->oDb->connect("Erro de connect em search::stopWords()");
			
			$lbOr = false;
			$lsSql = "SELECT stWord FROM stopWords WHERE isActive='1'";
			
			$lsWhere = "";

			foreach ($laQuery as $lsWord)
			{
				if ($lbOr == true)
				{
					$lsWhere .= " OR";
				}
				
				$lsWhere .= "stWord='" . trim($lsWord) . "'";
				$lbOr = true;
			}
			
			if (! empty($lsWhere))
			{
				$lsSql .= " AND ($lsWhere)";
			}
			
			$lrRes = $this->oDb->query($lsSql, "Erro na query em search::stopWords()");
			
			while ($loRec = $this->oDb->fetch_object($lrRes))
			{
				foreach ($laQuery as $lnK => $lsWord)
				{
					if ($lsWord == $loRec->stWord)
					{
						unset($laQuery[$lnK]);
					}
				}
			}
		}
		
		$this->aBoolWords = $laQuery;
	}

	
	/**
	 * 
	 */
	protected function setSearchType () : void
	{
		$lsQuery = $this->sOriginalQuery;
		
		if (empty($lsQuery))
		{
			return;
		}
		
		if (preg_match('/^\\\".*\\\"$/i', $lsQuery))
		{
			$this->sSearchType = "expression";
			$this->sExpression = $lsQuery;
		}
		else
		{
			$laQuery = explode(" ", $lsQuery);
			
			$lsWithWordsX = "";
			$lsWithoutWords = "";
			$lsWithWords = "";
			
			foreach ($laQuery as $lsWord)
			{
				$lsSignal = substr($lsWord, 0, 1);
				
				if ($lsSignal == "-")
				{
					$lsWithoutWords .= substr($lsWord, 1) . " ";
				}
				elseif ($lsSignal == "+")
				{
					$lsWithWords .= substr($lsWord, 1) . " ";
				}
				else
				{
					if (! empty($lsWord))
					{
						$lsWithWordsX .= $lsWord . " ";
					}
				}
			}

			$lsWithWords = trim($lsWithWords);
			$lsWithoutWords = trim($lsWithoutWords);
			
			if (! empty($lsWithoutWords) || ! empty($lsWithWords))
			{
				$this->sSearchType = "specific";
				$lsWithWords .= $lsWithWordsX;
				$this->aWithWords = !empty($lsWithWords) ? explode(" ", $lsWithWords) : "";
				$this->aWithoutWords = !empty($lsWithoutWords) ? explode(" ", $lsWithoutWords) : "";
			}
			
			if (empty($this->sSearchType))
			{
				$laAndBoolean = explode(" AND ", $lsQuery);
				$lsAndWords = "";
				$lsOrWords = "";
				
				if (count($laAndBoolean) > 1)
				{
					$lsOrBoolean = $laAndBoolean[0];
					unset($laAndBoolean[0]);
					
					foreach ($laAndBoolean as $lsWord)
					{
						$laWords = explode(" ", $lsWord);
						$lsAndWords .= $laWords[0] . " ";
						
						foreach ($laWords as $lnKey => $lsKeyWord)
						{
							if ($lnKey != 0 && ! empty($lsKeyWord))
							{
								$lsOrWords .= $lsKeyWord . " ";
							}
						}
					}
					
					$this->aOrWords = explode(" ", trim($lsOrWords . $lsOrBoolean));
					$this->aAndWords = explode(" ", trim($lsAndWords));
				}
				else
				{
					$lsQuery = trim($lsQuery);
					$laWords = explode(" ", $lsQuery);
					
					if (is_array($laWords))
					{
						foreach ($laWords as $lsWord)
						{
							if (! empty($lsWord))
							{
								$this->aOrWords[] = $lsWord;
							}
						}
					}
				}
				
				$this->sSearchType = "boolean";
			}
		}
	}

	
	/**
	 * 
	 */
	protected function cutString () : void
	{
		switch ($this->sSearchType)
		{
			case "expression":
				if (strlen($this->sExpression) > $this->nMaxLenQuery)
				{
					$this->sExpression = ltrim(substr($this->sExpression, 0, $this->nMaxLenQuery - 1)) . '"';
				}
			
			break;
			case "specific":
				if (is_array($this->aWithWords))
				{
					foreach ($this->aWithWords as $lsWord)
					{
						$lnLength = strlen($lsWord) + 2;
						
						if ($lnLength < $this->nMaxLenQuery)
						{
							$laWithWords[] = $lsWord;
						}
					}
					
					$this->aWithWords = $laWithWords;
				}
				
				if (is_array($this->aWithoutWords))
				{
					foreach ($this->aWithoutWords as $lsWord)
					{
						$lnLength = strlen($lsWord) + 2;
						
						if ($lnLength < $this->nMaxLenQuery)
						{
							$laWithoutWords[] = $lsWord;
						}
					}
					
					$this->aWithoutWords = $laWithoutWords;
				}
			
			break;
			case "boolean":
				if (is_array($this->aAndWords))
				{
					foreach ($this->aAndWords as $lsWord)
					{
						$lnLength = strlen($lsWord) + 2;
						
						if ($lnLength < $this->nMaxLenQuery)
						{
							$laAndWords[] = $lsWord;
						}
					}
					
					$this->aAndWords = $laAndWords;
				}
				
				if (is_array($this->aOrWords))
				{
					foreach ($this->aOrWords as $lsWord)
					{
						$lnLength = strlen($lsWord) + 2;
						
						if ($lnLength < $this->nMaxLenQuery)
						{
							$laOrWords[] = $lsWord;
						}
					}
					
					$this->aOrWords = $laOrWords;
				}
		}
	}

	
	/**
	 * 
	 */
	protected function createRLikeTerm () : void
	{
		if ($this->sSearchType == "expression")
		{
			$this->sQuery = strtr($this->sExpression, ['\"' => '']);
		}
		elseif ($this->sSearchType == "specific")
		{
			if (is_array($this->aWithWords))
			{
				foreach ($this->aWithWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "($lsWord)+";
				}
			}
			
			if (is_array($this->aWithoutWords))
			{
				foreach ($this->aWithoutWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "($lsWord)";
				}
			}
		}
		elseif ($this->sSearchType == "boolean")
		{
			$lsOr = "";
			
			if (is_array($this->aAndWords))
			{
				foreach ($this->aAndWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "($lsWord)+.*";
					$lsOr = "|";
				}
			}
			
			if (is_array($this->aOrWords))
			{
				foreach ($this->aOrWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "$lsOr($lsWord.*)";
					$lsOr = "|";
				}
			}
		}
	}

	
	/**
	 * 
	 * @param string $psQuery
	 * @return string|bool
	 */
	public function createRLikeQuery (string $psQuery)
	{	
		$this->sOriginalQuery = trim(Str::escapeString($psQuery));
		$lsWhere = "";

		if (empty($this->sOriginalQuery))
		{
			$this->nError = '1';
			return false;
		}
		elseif (! empty($this->sOriginalQuery))
		{
			$this->setSearchType();
			
			//$this->stopWords();
			
			$this->cutString();
			
			$this->createRLikeTerm();

			if ($this->sSearchType != "expression")
			{
				$this->sQuery = "RLIKE '{$this->sQuery}'";
			}
			else
			{
				$this->sQuery = str_replace("\'", "", $this->sQuery);
				$this->sQuery = "LIKE '%{$this->sQuery}%'";
			}
			
			if (is_array($this->mSearchFields))
			{
				$lsOr = "";
				
				foreach ($this->mSearchFields as $lsField)
				{
					$lsWhere .= $lsOr . "{$lsField} {$this->sQuery}";
					$lsOr = " OR ";
				}
			}
			elseif (! empty($this->mSearchFields))
			{
				$lsWhere = "{$this->mSearchFields} {$this->sQuery}";
			}
		}
		
		return $this->sWhere = $lsWhere;
	}


	/**
	 * 
	 */
	protected function createFullTextTerm () : void
	{
		if ($this->sSearchType == "expression")
		{
			$this->sQuery = strtr($this->sExpression, ['\"' => '']);
		}
		elseif ($this->sSearchType == "specific")
		{
			if (is_array($this->aWithWords))
			{
				foreach ($this->aWithWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "+$lsWord ";
				}
			}
			
			if (is_array($this->aWithoutWords))
			{
				foreach ($this->aWithoutWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "-$lsWord ";
				}
			}
		}
		elseif ($this->sSearchType == "boolean")
		{		
			if (is_array($this->aAndWords))
			{
				foreach ($this->aAndWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "+$lsWord* ";
				}
			}
			
			if (is_array($this->aOrWords))
			{
				foreach ($this->aOrWords as $lsWord)
				{
					$lsWord = Str::clearSignals($lsWord);
					$this->sQuery .= "$lsWord* ";
				}
			}
		}

		$this->sQuery = trim($this->sQuery);
	}	

	
	/**
	 * 
	 * @param string $psQuery
	 * @return bool|string
	 */
	public function createFullTextQuery (string $psQuery)
	{
		$this->sOriginalQuery = trim(Str::escapeString($psQuery));

		if (empty($this->sOriginalQuery))
		{
			$this->nError = '1';
			return false;
		}
		elseif (! empty($this->sOriginalQuery))
		{
			$this->setSearchType();
				
			//$this->stopWords();
				
			$this->cutString();
				
			$this->createFullTextTerm();
				
			if ($this->sSearchType != "expression")
			{
				$this->sQuery = "'{$this->sQuery}' IN BOOLEAN MODE";
			}
			else
			{
				$this->sQuery = "'{$this->sQuery}'";
			}	
				
			if (is_array($this->mSearchFields))
			{
				$this->mSearchFields = implode(", ", $this->mSearchFields);
			}

			if (! empty($this->mSearchFields))
			{
				$this->sWhere = "MATCH ({$this->mSearchFields}) AGAINST ({$this->sQuery})";
			}
		}
	
		return $this->sWhere;
	}


	/**
	 * 
	 * @param string $psQuery
	 * @param float $pnRelevance
	 * @return bool|string
	 */
	public function createDoctrineFullTextQuery (string $psQuery, float $pnRelevance = 0.1)
	{
		$this->sOriginalQuery = trim(Str::escapeString($psQuery));

		if (empty($this->sOriginalQuery))
		{
			$this->nError = '1';
			return false;
		}
		elseif (! empty($this->sOriginalQuery))
		{
			$this->setSearchType();

			//$this->stopWords();
				
			$this->cutString();
				
			$this->createFullTextTerm();
				
			if ($this->sSearchType != "expression")
			{
				$this->sQuery = "'{$this->sQuery}'";
				$lsMode = "'IN BOOLEAN MODE'";
			}
			else
			{
				$this->sQuery = "'{$this->sQuery}'";
				$lsMode = "'IN NATURAL MODE'";
			}
							
			if (is_array($this->mSearchFields))
			{
				$this->mSearchFields = implode(", ", $this->mSearchFields);
			}
			
			if (! empty($this->mSearchFields))
			{
				$this->sSelect = "MATCH_AGAINST ({$this->mSearchFields}, {$this->sQuery} {$lsMode})";
				$this->sWhere = "MATCH_AGAINST ({$this->mSearchFields}, {$this->sQuery}) > {$pnRelevance}";
			}
		}

		return $this->sWhere;
	}
}