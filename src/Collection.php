<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use ArrayIterator;


class Collection
{
	/**
	 * The source data
	 *
	 * @var array
	 */
	protected $aData = [];
	
	
	/**
	 * Create new collection
	 *
	 * @param array $paItems Pre-populate collection with this key-value array
	 */
	public function __construct (array $paItems = [])
	{
		return $this->populate($paItems);
	}

	
	/**
	 * Set collection item
	 *
	 * @param string $psKey The data key
	 * @param mixed $pmValue The data value
	 * @throws \Exception
	 */
	public function __set (string $psKey, $pmValue)
	{
		throw new \Exception("Unable to set vars dynamically");
	}
	
	
	/**
	 * Set collection item
	 *
	 * @param string $psKey The data key
	 * @param mixed $pmValue The data value
	 */
	public function set (string $psKey, $pmValue) : void
	{
		$this->aData[$psKey] = $pmValue;
	}
	
	
	/**
	 * Get collection item for key
	 *
	 * @param string $psKey The data key
	 * @param mixed $pmDefault The default value to return if data key does not exist
	 *
	 * @return mixed The key's value, or the default value
	 */
	public function get (string $psKey, $pmDefault = null)
	{
		if ($this->has($psKey))
		{
			return $this->aData[$psKey];
		}
		
		return $pmDefault;
	}
	
	
	/**
	 * Add item to collection
	 *
	 * @param array $paItems
	 */
	public function populate (array $paItems) : Collection
	{
		foreach ($paItems as $lsKey => $lmValue) 
		{
			$this->set($lsKey, $lmValue);
		}
		
		return $this;
	}
	
	
	/**
	 * Get all items in collection
	 *
	 * @return array The collection's source data
	 */
	public function all () : array
	{
		return $this->aData;
	}
	
	
	/**
	 * Get collection keys
	 *
	 * @return array The collection's source data keys
	 */
	public function keys () : array
	{
		return array_keys($this->aData);
	}
	
	
	/**
	 * Does this collection have a given key?
	 *
	 * @param string $psKey
	 * @return bool
	 */
	public function has (string $psKey) : bool
	{
		return array_key_exists($psKey, $this->aData);
	}
	
	
	/**
	 * Remove a item from collection
	 *
	 * @param string $psKey
	 */
	public function remove (string $psKey) : void
	{
		unset($this->aData[$psKey]);
	}
	
	
	/**
	 * Remove all items from collection
	 */
	public function clear () : void
	{
		$this->aData = [];
	}
	
	
	/**
	 * Get number of items in collection
	 *
	 * @return int
	 */
	public function count () : int
	{
		return count($this->aData);
	}
	
	
	/**
	 * Get collection iterator
	 *
	 * @return \ArrayIterator
	 */
	public function getIterator () : ArrayIterator
	{
		return new ArrayIterator($this->aData);
	}
	
	
	/**
	 *
	 * @param array|null $paArray
	 * @param string $psIdentation
	 * @throws Exception
	 * @return string
	 */
	public function arrayToString (?array $paArray = null, string $psIdentation = "\t") : string
	{
		if (null !== $paArray)
		{
			$paArray = $this->aData;
		}
		
		if (is_array($paArray))
		{
			$lsString = "";
			
			foreach ($paArray as $lsKey => $lmValue)
			{
				if (is_array($lmValue))
				{
					$lsString .= $psIdentation . "'$lsKey' => [\n" . $this->arrayToString($lmValue, $psIdentation . "\t") . $psIdentation ."],\n";
				}
				elseif (is_bool($lmValue))
				{
					$lsString .= $psIdentation . "'$lsKey' => " . ($lmValue ? 'true' : 'false') . ",\n";
				}
				elseif ($lmValue === null)
				{
					$lsString .= $psIdentation . "'$lsKey' => null,\n";
				}
				else
				{
					if (preg_match("/\/\/array/", $lmValue))
					{
						$lsString .= $psIdentation . "'$lsKey' => " . substr($lmValue, 2) . ",\n";
					}
					else
					{
						$lsString .= $psIdentation . "'$lsKey' => '$lmValue',\n";
					}
				}
			}
			
			return $lsString;
		}
		else
		{
			throw new \Exception("The param value should be an array!");
		}
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function arrayToFile () : string
	{
		return "<?php\nreturn [\n" . $this->arrayToString() . "];";
	}
	
	
	/**
	 * Merge options recursively
	 *
	 * @param array|null $paArrayToMerge
	 * @return \OnionLib\Collection
	 */
	public function merge (?array $paArrayToMerge = null) : Collection
	{
		if (is_array($paArrayToMerge))
		{
			foreach ($paArrayToMerge as $lmKey => $lmValue)
			{
				if (is_array($paArrayToMerge[$lmKey]))
				{
					if ($this->has($lmKey) && is_array($this->aData[$lmKey]))
					{
						$this->aData[$lmKey] = $this->merge($this->aData[$lmKey], $paArrayToMerge[$lmKey]);
					}
					else 
					{
						$this->aData[$lmKey] = $paArrayToMerge[$lmKey];
					}
				}
				else
				{
					$this->aData[$lmKey] = $lmValue;
				}
			}
		}
		
		return $this;
	}
}