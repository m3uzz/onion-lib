<?php
 /**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class AuthContext
{
	/**
	 * @var array
	 */
	protected $aVHostIp = [];

	
	/**
	 * Se não houver nenhum contexto, o padrão é tudo liberado.
	 * Se houver ao menos 1 contexto, o padrão se torna tudo negado.
	 * As regras serão percorridas e testadas até encontrar uma que permita o acesso, saíndo lo looping.
	 * 
	 * @param array|null $paContext
	 * @param array $paVHosts
	 * @param array $paVHostIp
	 * @return bool
	 */
	public function isInContext (?array $paContext = null, array $paVHosts = [], ?array &$psVHostIp = []) : bool
	{
		$lsUserIp = (isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''));
		$lsAppClient = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
		$lsToken = isset($_SERVER['HTTP_TOKEN']) ? $_SERVER['HTTP_TOKEN'] : "";
		$lbReturn = true;
	
		Debug::debug($_SERVER);
		Debug::debug([$lsUserIp, $lsAppClient, $lsToken]);
		Debug::debug($paContext);
	
		if (is_array($paContext))
		{
			$lbReturn = false;

			foreach ($paContext as $lsIp => $laIpContext)
			{
				$this->checkVirtualHost($lsIp, $laIpContext, $paVHosts);
				$psVHostIp = $this->aVHostIp;

                $lsIpPattern = preg_replace(["/^!([\d\.?]*)$/", "/\./", "/\?/"], ["[^$1]", "\.", "[\d]*"], $lsIp);
                Debug::debug($lsIp);
                Debug::debug($lsIpPattern);
				
				if (preg_match("/^$lsIpPattern$/", $lsUserIp))
				{
					Debug::debug("IP: $lsUserIp");

					if (isset($laIpContext['denied']) && $laIpContext['denied'] == true)
					{
						$lbReturn = false;
						Debug::debug("IP [$lsUserIp] DENIED FOR $lsIpPattern");
						continue;
					}
                    
                    if (isset($laIpContext['user-agent']) && count($laIpContext['user-agent']) >0)
                    {
                        if (isset($laIpContext['user-agent'][$lsAppClient]))
                        {
                            Debug::debug("USER-AGENT: $lsAppClient");
                                
                            if (!empty($laIpContext['user-agent'][$lsAppClient]) && $laIpContext['user-agent'][$lsAppClient] != $lsToken)
                            {
                                Debug::debug("TOKEN DENIED: $lsToken");
                                $lbReturn = false;
                                continue;
                            }
                        }
                        elseif (isset($laIpContext['user-agent']['*']))
                        {
                            Debug::debug("USER-AGENT: *");
                                
                            if (!empty($laIpContext['user-agent']['*']) && $laIpContext['user-agent']['*'] != $lsToken)
                            {
                                Debug::debug("TOKEN DENIED: $lsToken");
                                $lbReturn = false;
                                continue;
                            }
                        }
                    }

					return true;
				}
				else 
				{
					Debug::debug("IP [$lsUserIp] DENIED FOR $lsIpPattern");
				}
			}
		}

		return $lbReturn;
	}


	/**
	 * 
	 * @param string $psIp
	 * @param array $paIpContext
	 * @param array $paVHosts
	 */
	private function checkVirtualHost (string &$psIp, array $paIpContext, array $paVHosts = [])
	{
		if (isset($paIpContext['host']) && !empty($paIpContext['host']))
		{
			if (isset($paVHosts[$paIpContext['host']]) && !empty($paVHosts[$paIpContext['host']])) 
			{
				$this->aVHostIp[$paIpContext['host']] = $paVHosts[$paIpContext['host']];
				$psIp = $paVHosts[$paIpContext['host']];
			}
		}
	}	
}