<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;

class Sitemap
{
	/**
	 * 
	 * @var OnionLib\Sitemap
	 */
	private static $oInstance;

	/**
	 * @var object
	 */
	protected $oDocument;

	/**
	 * @var bool
	 */
	protected $bLoadOK;

	/**
	 * @var string
	 */
	protected $sXmlEncoding = "UTF-8";

	/**
	 * @var string
	 */
	protected $sChangeFreq = "monthly";

	/**
	 * @var string
	 */
	protected $sPriority = "0.5";
	
	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance () : Sitemap
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self;
		}
		
        return self::$oInstance;
	}


	/**
	 * 
	 */
	private function __construct ()
	{
		$this->oDocument = new \DOMDocument();
	}

	
	/**
	 *
	 * @param string $psXml
	 * @param string $psMsgError
	 * @return bool
	 */
	public function xmlOpen (string $psXml, string $psMsgError = "XML error") : bool
	{
		if (!$this->oDocument->loadXML($psXml, LIBXML_COMPACT))
		{
			$this->bLoadOK = false;
			return false;
		}
		else
		{
			$this->bLoadOK = true;
			return true;
		}
	}
	

	/**
	 *
	 * @param string $psTag
	 * @param object $poObject
	 * @return object
	 */
	public function xmlById (string $psTag, &$poObject = null) : object
	{
		$loList = $this->xmlByTagName($psTag, $poObject);
		return $this->xmlGetItem($loList);
	}

	
	/**
	 *
	 * @param string $psTag
	 * @param object $poObject
	 * @return object
	 */
	public function xmlByTagName (string $psTag, &$poObject = null) : object
	{
		if ($poObject==null)
		{
			$poObject = &$this->oDocument;
		}

		if ($this->bLoadOK)
		{
			$loList = $poObject->getElementsByTagName($psTag);

			if (is_object($loList) && $loList->length != 0)
			{
				return $loList;
			}
			else
			{
				return null;
			}
		}
	}
	

	/**
	 *
	 * @param object $poNodeList
	 * @param string $psIndex
	 * @return null|object
	 */
	public function xmlGetItem (&$poNodeList, string $psIndex = '0') : object
	{
		if (is_object($poNodeList))
		{
			return $poNodeList->item($psIndex);
		}
		else
		{
			return null;
		}
	}

	
	/**
	 *
	 * @param object $poElement
	 * @param string $psAttribute
	 * @return string
	 */
	public function xmlGetAttribute (&$poElement, string $psAttribute) : string
	{
		if ($poElement->hasAttribute($psAttribute))
		{
			return mb_convert_encoding($poElement->getAttribute($psAttribute), 'iso8859-1', 'UTF-8');
		}
	}

	
	/**
	 *
	 * @param object $poElement
	 * @return string
	 */
	function xmlGetValue (&$poElement) : string
	{
		return str_replace("&amp;", "&", urldecode(mb_convert_encoding((is_object($poElement) ? $poElement->nodeValue : ""), 'iso8859-1', 'UTF-8')));
	}
	

	/**
	 *
	 * @param string $psElement
	 * @param string $psValue
	 * @param array|null $paAttr
	 * @return string
	 */
	public function setElement (string $psElement, string $psValue, ?array $paAttr = null) : string
	{
		$lsAttributs = $this->setAttribut($paAttr);

		return "<$psElement $lsAttributs>\n$psValue\n</$psElement>\n";
	}
	

	/**
	 *
	 * @param array|null $paAttr
	 * @return string
	 */
	public function setAttribut (?array $paAttr = null) : string
	{
		$lsAttributs = "";

		if (is_array($paAttr))
		{
			foreach ($paAttr as $lsAttr => $lsValue)
			{
				$lsAttributs .= $lsAttr.'="'.$lsValue.'" ';;
			}
		}

		return $lsAttributs;
	}
	

	/**
	 * @param string $psValue
	 * @param string $psField
	 * @param string $psType
	 * @return string
	 */
	public function cdata (string $psValue, string $psField = '', string $psType = '') : string
	{
		$lnLen = strlen($psValue);

		for ($lnX = 0; $lnX<$lnLen; $lnX++)
		{
			if (ord($psValue[$lnX]) < 32)
			{
				$psValue[$lnX] = " ";
			}
		}

		if ($psType == "rss")
		{
			if ($psField == "description" || $psField == "title" || $psField == "author")
			{
				return "<![CDATA[".$psValue."]]>";
			}
			else
			{
				return preg_replace("/&/", "&amp;", $psValue);
			}
		}
		elseif (!empty($psValue))
		{
			return "<![CDATA[$psValue]]>";
		}
	}

	
	/**
	 *
	 * @param array $paLinks
	 * @return array
	 */
	public function setSitemap (array $paLinks = []) : array
	{
		$lnId0 = 0;

		$laSitemap['header'] = '<?xml version="1.0" encoding="'.$this->sXmlEncoding.'"?>';
		$laSitemap[$lnId0]['conteiner'] = 'urlset';
		$laSitemap[$lnId0]['attr']['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';
		$laSitemap[$lnId0]['attr']['xsi:schemaLocation'] = 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd';
		$laSitemap[$lnId0]['attr']['xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';

		$lnId1 = 0;

		foreach ($paLinks as $lnKey => $lsLink)
		{
			$laItem['conteiner'] = 'url';
			$laValue[0]['conteiner'] = 'loc';
			$laValue[0]['value'] = $lsLink;
			$laValue[1]['conteiner'] = 'lastmod';
			$laValue[1]['value'] = date('Y-m-dTH:i:s+00:00');
			$laValue[2]['conteiner'] = 'changefreq';
			$laValue[2]['value'] = $this->sChangeFreq;
			$laValue[3]['conteiner'] = 'priority';
			$laValue[3]['value'] = $this->sPriority;
				
			$laItem['value'] = $laValue;
			$laSitemap[$lnId0]['value'][$lnId1] = $laItem;
				
			$lnId1++;
		}

		return $laSitemap;
	}

	
	/**
	 *
	 * @param array $paSitemap
	 * @return string
	 */
	public function generateSitemap (array $paSitemap) : string
	{
		$lsSitemapXml = "";

		if (is_array($paSitemap))
		{
			if (isset($paSitemap['header']))
			{
				$lsSitemapXml = $paSitemap['header'] . "\n";
				unset($paSitemap['header']);
			}
				
			if (is_array($paSitemap))
			{
				foreach ($paSitemap as $lnK => $laItem)
				{
					$lsSitemapXml .= $this->setXml($laItem);
				}
			}
		}

		return $lsSitemapXml;
	}

	
	/**
	 *
	 * @param array $paItem
	 * @return string
	 */
	public function setXml (array $paItem) : string
	{
		$laAttr = null;
		$lsConteiner = null;
		$lsValue = null;
			
		if (isset($paItem['conteiner']))
		{
			$lsConteiner = $paItem['conteiner'];
		}

		if (isset($paItem['attr']) && is_array($paItem['attr']))
		{
			$laAttr = $paItem['attr'];
		}

		if (isset($paItem['value']))
		{
			if (is_array($paItem['value']))
			{
				foreach ($paItem['value'] as $lnK => $laItem)
				{
					$lsValue .= $this->setXml($laItem);
				}
			}
			else
			{
				$lsValue = $paItem['value'];
			}
		}

		return $this->setElement($lsConteiner, $lsValue, $laAttr);
	}

	
	/**
	 *
	 * @param string $psXML
	 */
	public function renderSitemap (string $psXML) : void
	{
		header("Content-type: text/xml; charset=".$this->sXmlEncoding);
		echo $psXML;
	}
}