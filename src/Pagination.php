<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 *
 *
 * Exemplo do arquivo template
 * 	<!--PAGINATION_PREV-->
 * 		 <a href="#%LINK%#">Previous</a>&nbsp;&nbsp;
 * 	<!--/PAGINATION_PREV-->
 * 	
 * 	<!--PAGINATION_NEXT-->
 * 		 <a href="#%LINK%#">Next</a>
 * 	<!--/PAGINATION_NEXT-->
 * 	
 * 	<!--PAGINATION_CURRENT-->
 * 		&nbsp;#%PG%#&nbsp;
 * 	<!--/PAGINATION_CURRENT-->
 * 	
 * 	<!--PAGINATION_PAGE-->
 * 		<a href="#%LINK%#">#%PG%#</a>&nbsp;
 * 	<!--/PAGINATION_PAGE-->
 * 	
 * 	<!--PAGINATION-->
 * 		<div>
 * 			#%PAGINATION%#
 * 		</div>
 * 	<!--/PAGINATION-->
 *
 *
 * Exemplo da montagem da array que será passada como parametro
 * $laPatination['pg_prev'] 	= '<a href="#%LINK%#">Previous</a>&nbsp;&nbsp;'."\n";
 * $laPatination['pg_next'] 	= '<a href="#%LINK%#">Next</a>'."\n";
 * $laPatination['pg_current'] 	= '&nbsp;#%PG%#&nbsp;'."\n";
 * $laPatination['pg_link'] 	= '<a href="#%LINK%#">#%PG%#</a>&nbsp;'."\n";
 * $laPatination['pagination'] 	= '<div>#%PAGINATION%#</div>';
 */
declare (strict_types = 1);

namespace OnionLib;


class Pagination
{

	/**
	 * Url para o link de paginação
	 * 
	 * @var string
	 */
	protected $sUri = "?";

	/**
	 * Nome da variável de paginação
	 * 
	 * @var string
	 */
	protected $sVarName = "p";

	/**
	 * Índice da primeira página, `0` ou `1`
	 * 
	 * @var int
	 */
	protected $nFirstPageIndex = 0;

	/**
	 * Intervalo para o índice de paginação
	 * 
	 * @var int
	 */
	protected $nRange = 1;

	/**
	 * Quantidade de registros a serem exibidos em uma página
	 * 
	 * @var int
	 */
	protected $nResPerPage = 10;

	/**
	 * Quantida de links de páginas a serem exibidos
	 * ex.: se 5 e o número de páginas for 10, será exibido 1 2 3 4 5 > >> ou <<
	 * < 4 5 6 7 8 > >>
	 * 
	 * @var int
	 */
	protected $nPaginatorLimit = 5;

	/**
	 * Retorno de setPaginator, array com todas as informações para a paginação
	 * 
	 * @var array
	 */
	public $aData = [];

	/**
	 *  
	 * @var array
	 */
	protected $aTemplate = null;

	
	/**
	 * Seta a propriedade $aTemplate com os valores padroes;
	 */
	public function __construct ()
	{
		$this->aTemplate['pg_first'] = '<li class="page-item"><a class="page-link" aria-label="First" href="#%LINK%#" data-act="#%L%#" data-page="#%P%#"><span aria-hidden="true">&laquo;</span></a></li>' . "\n";
		$this->aTemplate['pg_last'] = '<li class="page-item"><a class="page-link" aria-label="Last" href="#%LINK%#" data-act="#%L%#" data-page="#%P%#"><span aria-hidden="true">&raquo;</span></a></li>' . "\n";
		$this->aTemplate['pg_prev'] = '<li class="page-item"><a class="page-link" aria-label="Previous" href="#%LINK%#" data-act="#%L%#" data-page="#%P%#"><span aria-hidden="true">&laquo;</span></a></li>' . "\n";
		$this->aTemplate['pg_next'] = '<li class="page-item"><a class="page-link" aria-label="Next" href="#%LINK%#" data-act="#%L%#" data-page="#%P%#"><span aria-hidden="true">&raquo;</span></a></li>' . "\n";
		$this->aTemplate['pg_current'] = '<li class="page-item active" aria-current="page"><a class="page-link" href="#">#%PG%# <span class="sr-only">(current)</span></a></li>' . "\n";
		$this->aTemplate['pg_link'] = '<li class="page-item"><a class="page-link" href="#%LINK%#" data-act="#%L%#" data-page="#%P%#">#%PG%#</a></li>' . "\n";
		$this->aTemplate['pagination'] = '<ul class="pagination">#%PAGINATION%#</ul>';
		
		$this->aData = [
			'prev' => 0,
			'next' => 0,
			'prev_url' => '',
			'next_url' => '',
			'current' => 0,
			'until' => 0,
			'total' => 0,
			'count_pages' => 0,
			'pg_current' => 0,
			'index' => [],
			'urls' => []
		];		
		
		return $this;
	}


	/**
	 * 
	 * @param string $psVar
	 * @param int|string $pmValue
	 */
	public function set (string $psVar, $pmValue) : Pagination
	{
		if (property_exists($this, $psVar))
		{
			$this->{$psVar} = $pmValue;
		}

		return $this;
	}

	
	/**
	 * 
	 * @param string|null $psVar
	 * @return mixed
	 */
	public function get (?string $psVar = null)
	{
		if ($psVar !== null)
		{
			if (property_exists($this, $psVar))
			{
				return $this->{$psVar};
			}
		}
		else 
		{
			return get_object_vars($this);
		}
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function toArray () : array
	{
		$laProperties = get_object_vars($this);
		
		$laReturn['_this'] = get_class($this);
		
		if (is_array($laProperties))
		{
			foreach ($laProperties as $lsKey => $lmValue)
			{
				$laReturn[$lsKey] = $lmValue;
			}
		}
		
		return $laReturn;
	}
	

	
	/**
	 * Gera um array com as informações para a paginação
	 * 
	 * @param int $pnCountRes total de registros
	 * @param int $pnCurrent índice da página atual
	 * @param array|null $paParams
	 */
	public function setPaginator (int $pnCountRes, int $pnCurrent = 0, ?array $paParams = null) : Pagination
	{
		// Verifica se o indice para a página atual está setada,
		// caso contrário seta com o valor de First Page.
		if (!isset($pnCurrent) || $pnCurrent == null)
		{
			$pnCurrent = $this->nFirstPageIndex;
		}
		
		// O índice da primeira página pode ser 0 ou 1
		// caso a primeira página seja 0, o índice a ser exibido inicia em 1.
		$lnIncrement = 1;
		
		// Caso o índice seja 1 o incremento é igual a 0
		if ($this->nFirstPageIndex == 1)
		{
			$lnIncrement = 0;
		}
		
		// calculando o índice para a página agual
		$lnPgCurrent = ($pnCurrent / ($this->nResPerPage / $this->nRange)) + $lnIncrement;
		
		// calculando o número de páginas
		$lnCountPageInit = $pnCountRes / $this->nResPerPage;
		$lnCountPages = (int) $lnCountPageInit;
		
		// Se o número de páginas for fracionado, é somada mais uma página
		if ($lnCountPages < $lnCountPageInit)
		{
			$lnCountPages = $lnCountPages + 1;
		}
		
		// Se o número de páginas for menor ou igual ao número de índices a
		// serem exibidos, então o índice inicial começa em 1
		if ($lnCountPages <= $this->nPaginatorLimit)
		{
			$lnInit = 1;
			$i = 1;
		}
		// Senão o índici inicial deve ser calculado
		else
		{
			// Movendo o índice inicial para exibir sempre a mesma quantidade de
			// índices
			$lnNewInit = $lnPgCurrent + (int) ($this->nPaginatorLimit / 2); // para range 10
			
			if ($lnNewInit <= $lnCountPages)
			{
				$i = $lnNewInit - $this->nPaginatorLimit;
				
				if ($i < 1)
				{
					$i = 1;
				}
			}
			else
			{
				$i = $lnCountPages - ($this->nPaginatorLimit - 1);
			}
		}
		
		$laIndex = [];
		$laUrls = [];
		
		for ($x = 0; $x < $this->nPaginatorLimit; $x ++)
		{
			// Criando os índices para a páginas a serem exibidas
			if (($i + $x) <= $lnCountPages)
			{
				$nn = (($i + $x - 1) * ($this->nResPerPage / $this->nRange)) + $this->nFirstPageIndex;
				
				$laIndex[$i + $x] = $nn;
				$laUrls[$i + $x] = $this->sUri . $this->sVarName . "=" . $nn;
				
				// verifica se possui parametros a serem concatenados
				if (is_array($paParams))
				{
					foreach ($paParams as $lmKey => $lmValue)
					{
						$laUrls[$i + $x] .= '&' . $lmKey . '=' . $lmValue;
					}
				}
			}
		}
		
		$lnUntil = ($pnCurrent * $this->nRange) + $this->nResPerPage;
		
		if ($lnUntil > $pnCountRes)
		{
			$lnUntil = $pnCountRes;
		}
		
		$lnPrev = 0;
		$lsPrevUrl = '';
		
		if ($lnPgCurrent > 1)
		{
			$lnPrev = $pnCurrent - ($this->nResPerPage / $this->nRange);
			$lsPrevUrl = $this->sUri . $this->sVarName . "=" . $lnPrev;
			
			// verifica se possui parametros a serem concatenados
			if (is_array($paParams))
			{
				foreach ($paParams as $lmKey => $lmValue)
				{
					$lsPrevUrl .= '&' . $lmKey . '=' . $lmValue;
				}
			}
		}
		
		$lnNext = 0;
		$lsNextUrl = '';
		
		if ($lnPgCurrent < $lnCountPages)
		{
			$lnNext = $pnCurrent + ($this->nResPerPage / $this->nRange);
			$lsNextUrl = $this->sUri . $this->sVarName . "=" . $lnNext;
			
			if (is_array($paParams))
			{
				foreach ($paParams as $lmKey => $lmValue)
				{
					$lsNextUrl .= '&' . $lmKey . '=' . $lmValue;
				}
			}
		}
		
		$lnFirst = $this->nFirstPageIndex;
		$lnLast = ($lnCountPages-1)*($this->nResPerPage / $this->nRange);

		$laData = [
			'first' => $lnFirst,
			'last' => $lnLast,
			'first_url' => "{$this->sUri}{$this->sVarName}={$lnFirst}",
			'last_url' => "{$this->sUri}{$this->sVarName}={$lnLast}",
			'prev' => $lnPrev,
			'next' => $lnNext,
			'prev_url' => $lsPrevUrl,
			'next_url' => $lsNextUrl,
			'current' => (int) ($pnCurrent), // + $lnIncrement,
			'until' => (int) $lnUntil,
			'total' => (int) $pnCountRes,
			'count_pages' => (int) $lnCountPages,
			'pg_current' => (int) $lnPgCurrent
		];

		$laData['index'] = $laIndex;
		$laData['urls'] = $laUrls;
		
		$this->aData = $laData;
		
		return $this;
	}

	
	/**
	 * Renderiza a paginação devolvendo o código html
	 * 
	 * @param string|null $psQuery query adicional para a url
	 * @return string
	 */
	public function renderPagination (?string $psQuery = null) : string
	{
		$psQuery = preg_replace("/\?/", "", (string)$psQuery);
		
		if (!empty($psQuery) && strpos($psQuery, '&', 0) !== 0)
		{
			$psQuery = '&' . $psQuery;
		}
		
		$lsPagination = "";

		if ($this->aData['count_pages'] > 1)
		{
			if (is_array($this->aData['index']))
			{
				if (! empty($this->aData['prev_url']))
				{
					$lsPattern = [
						"/#%LINK%#/", 
						"/#%L%#/", 
						"/#%P%#/"
					];

					$this->aData['prev_url'] .= $psQuery;

					$lsReplaceF = [
						$this->aData['first_url'],
						$this->sUri,
						$this->aData['first'],
					];

					$lsReplaceP = [
						$this->aData['prev_url'],
						$this->sUri,
						$this->aData['prev'],
					];
					
					$lsPagination .= preg_replace($lsPattern, $lsReplaceF, $this->aTemplate['pg_first']);
					//$lsPagination .= preg_replace($lsPattern, $lsReplaceP, $this->aTemplate['pg_prev']);
				}
				
				foreach ($this->aData['urls'] as $lnKey => $lsUrlPg)
				{
					if ($lnKey != 0)
					{
						$this->aData['urls'][$lnKey] .= $psQuery;

						if ($this->aData['pg_current'] == $lnKey)
						{
							$lsPattern = "/#%PG%#/";
							$lsReplace = $lnKey;
							$lsPagination .= preg_replace($lsPattern, (string)$lsReplace, (string)$this->aTemplate['pg_current']);
						}
						else
						{
							$lsPattern = [
								"/#%LINK%#/",
								"/#%PG%#/",
								"/#%L%#/",
								"/#%P%#/"
							];

							$lsReplace = [
								$this->aData['urls'][$lnKey],
								$lnKey,
								$this->sUri,
								$this->aData['index'][$lnKey]
							];
							
							$lsPagination .= preg_replace($lsPattern, $lsReplace, $this->aTemplate['pg_link']);
						}
					}
				}
				
				if (! empty($this->aData['next_url']))
				{
					$lsPattern = [
						"/#%LINK%#/",
						"/#%L%#/",
						"/#%P%#/"
					];
						
					$this->aData['next_url'] .= $psQuery;

					$lsReplaceN = [
						$this->aData['next_url'],
						$this->sUri,
						$this->aData['next'],
					];

					$lsReplaceL = [
						$this->aData['last_url'],
						$this->sUri,
						$this->aData['last'],
					];

					//$lsPagination .= preg_replace($lsPattern, $lsReplaceN, $this->aTemplate['pg_next']);
					$lsPagination .= preg_replace($lsPattern, $lsReplaceL, $this->aTemplate['pg_last']);
				}
			}
		}
		
		$lsPattern = "/#%PAGINATION%#/";
		$lsReplace = $lsPagination;
		$lsPagination = preg_replace($lsPattern, $lsReplace, $this->aTemplate['pagination']);
		
		return $lsPagination;
	}
}