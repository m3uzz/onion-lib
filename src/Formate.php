<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class Formate
{
	
	/**
	 * 
	 * @param string|float|int|null $pmValue
	 * @return string
	 */
	public static function valorPorExtenso ($pmValue = 0) : string
	{
		$laSingular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
		$laPlural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões"];
	
		$laCentena = ["", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
		$laDezena = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa"];
		$laDezVinte = ["dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove"];
		$laUnitario = ["", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove"];
	
		$lnZ = 0;
		$lsRt = "";
	
		$pmValue = number_format($pmValue, 2, ".", ".");
		$laInteiro = explode(".", $pmValue);
	
		for($lnI=0; $lnI<count($laInteiro); $lnI++)
		{
			for($lnII=strlen($laInteiro[$lnI]); $lnII<3; $lnII++)
			{
				$laInteiro[$lnI] = "0" . $laInteiro[$lnI];
			}
		}
	
		// $fim identifica onde que deve se dar junção de centenas por "e" ou por ","
		$lnFim = count($laInteiro) - ($laInteiro[count($laInteiro)-1] > 0 ? 1 : 2);
	
		for ($lnI=0; $lnI<count($laInteiro); $lnI++)
		{
			$pmValue = $laInteiro[$lnI];
			$lsRc = (($pmValue > 100) && ($pmValue < 200)) ? "cento" : $laCentena[$pmValue[0]];
			$lsRd = ($pmValue[1] < 2) ? "" : $laDezena[$pmValue[1]];
			$lsRu = ($pmValue > 0) ? (($pmValue[1] == 1) ? $laDezVinte[$pmValue[2]] : $laUnitario[$pmValue[2]]) : "";
	
			$lsR = $lsRc . (($lsRc && ($lsRd || $lsRu)) ? " e " : "") . $lsRd . (($lsRd && $lsRu) ? " e " : "") . $lsRu;
			$lnT = count($laInteiro) - 1 - $lnI;
			$lsR .= $lsR ? " ".($pmValue > 1 ? $laPlural[$lnT] : $laSingular[$lnT]) : "";
	
			if ($pmValue == "000")
			{
				$lnZ++;
			}
			elseif ($lnZ > 0)
			{
				$lnZ--;
			}
	
			if (($lnT == 1) && ($lnZ > 0) && ($laInteiro[0] > 0))
			{
				$lsR .= (($lnZ>1) ? " de " : "") . $laPlural[$lnT];
			}
	
			if ($lsR)
			{
				$lsRt = $lsRt . ((($lnI > 0) && ($lnI <= $lnFim) && ($laInteiro[0] > 0) && ($lnZ < 1)) ? ( ($lnI < $lnFim) ? ", " : " e ") : " ") . $lsR;
			}
		}
	
		return ($lsRt ? $lsRt : "zero");
	}


	/**
	 * 
	 * @param string|int|float $pmValue
	 * @return float
	 */
	public static function cleanCurrence ($pmValue) : float
	{
		if (is_string($pmValue))
		{
			$lnDotPos = strrpos($pmValue, '.');
			$lnCommaPos = strrpos($pmValue, ',');

			$lbSep = (($lnDotPos > $lnCommaPos) && $lnDotPos) ? $lnDotPos :
				((($lnCommaPos > $lnDotPos) && $lnCommaPos) ? $lnCommaPos : false);

			if (!$lbSep) 
			{
				return floatval(preg_replace("/[^0-9]/", "", $pmValue));
			}
		
			return floatval(preg_replace("/[^0-9]/", "", substr($pmValue, 0, $lbSep)) . '.' .
				preg_replace("/[^0-9]/", "", substr($pmValue, $lbSep+1, strlen($pmValue))));
		}

		return (float)$pmValue;
	}

	
	/**
	 * 
	 * @param string|float|int|null $pmValue
	 * @param int $pnDec
	 * @return string
	 */
	public static function numberFormat ($pmValue = null, int $pnDec = 2) : string
	{		
		$pmValue = round((float)$pmValue, $pnDec);
		$lsResult = sprintf("%.{$pnDec}f", $pmValue);

		return $lsResult;
	}


	/**
	 * 
	 * @param string|float|int|null $pmValue
	 * @param int $pnDec
	 * @param bool $pbRound
	 * @return string|null
	 */
	public static function currenceFormat ($pmValue = null, int $pnDec = 2, bool $pbRound = false) : ?string
	{
		$pmValue = (string)$pmValue;

		if (trim($pmValue) !== "")
		{
			$lsLocale = locale_get_default();
			$loFmt = new \NumberFormatter($lsLocale, \NumberFormatter::CURRENCY);
			$lsSymbol = $loFmt->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
			$lsDecimalSep = $loFmt->getSymbol(\NumberFormatter::MONETARY_SEPARATOR_SYMBOL);
			$lsThousandSep = $loFmt->getSymbol(\NumberFormatter::MONETARY_GROUPING_SEPARATOR_SYMBOL);

			preg_match("/([\d\.,-]+)/", $pmValue, $laValue);
			$lsValue = preg_replace("/,/", "", $laValue[0]);
			
			if ($pbRound)
			{
				$lsValue = self::cleanCurrence($lsValue);
				$lsValue = (string)round($lsValue, $pnDec);
			}
			
			$laValue = explode(".", $lsValue);
			$lsInt = $laValue[0];
			$lsDec = isset($laValue[1]) ? $laValue[1] : "00";
			
			$laInt = str_split($lsInt);
			$lsSignal = "";
			
			if (isset($laInt[0]) && ($laInt[0] == "-"))
			{
				$lsSignal = $laInt[0];
				unset($laInt[0]);
			}
			
			$lsIntX = "";
			
			if (is_array($laInt))
			{
				$laInt = array_reverse($laInt);
				$lsPonto = "";
				$lnCount = 0;
				
				foreach ($laInt as $lnV)
				{
					$lsIntX .= $lsPonto . $lnV;
					$lnCount ++;
					$lsPonto = "";
					
					if ($lnCount == 3)
					{
						$lsPonto = $lsThousandSep;
						$lnCount = 0;
					}
				}
				
				$laValueX = str_split($lsIntX);
				$laValueX = array_reverse($laValueX);
				$lsInt = implode("", $laValueX);
						
				if ($pnDec > 0)
				{
					$lsDec .= "000000000"; 
					$lsDec = substr($lsDec, 0, $pnDec);
				}

				$lsValue = "{$lsSignal} {$lsSymbol} {$lsInt}{$lsDecimalSep}{$lsDec}";
			}


			
			return $lsValue;
		}
		
		return null;
	}
	
	
	/**
	 * Limpa ou formata um numero telefonico
	 * 
	 * @param string $psPhone
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara]
	 * @return string
	 */
	public static function formatPhone (string $psPhone, int $pnType = 0) : string
	{
		switch ($pnType)
		{
			case 1:
				$lsPhone = self::formatPhone($psPhone);
				return preg_replace('/^([0-9]{2})([0-9]{4,5})([0-9]{4})$/', '($1) $2-$3', $lsPhone);
				break;
			case 2:
				$lsPhone = self::formatPhone($psPhone);
				return preg_replace('/^([0-9]{2})([0-9]{4,5})([0-9]{4})$/', '($1) ****-$3', $lsPhone);
				break;
			default:
				return preg_replace("/[^0-9]/", "", $psPhone);
		}
	}


	/**
	 * Verifica se o numero de telefone é mobile ou não
	 * 
	 * @param string $psPhone
	 * @return bool
	 */
	public static function phoneIsMobile (string $psPhone) : bool
	{
		$lsPhone = self::formatPhone($psPhone);

		if (preg_match('/^[0-9]{2}9[0-9]{8}$/', $lsPhone) === 1)
		{
			return true;
		}

		return false;
	}


	/**
	 * Verifica se o numero de telefone é fixo ou não
	 * 
	 * @param string $psPhone
	 * @return bool
	 */
	public static function phoneIsFix (string $psPhone) : bool
	{
		$lsPhone = self::formatPhone($psPhone);

		if (preg_match('/^[0-9]{2}[2-5]{1}[0-9]{7}$/', $lsPhone) === 1)
		{
			return true;
		}

		return false;
	}


	/**
	 * Verifica se o numero de telefone é fixo ou não
	 * 
	 * @param string $psPhone
	 * @return bool
	 */
	public static function phoneIsService (string $psPhone) : bool
	{
		$lsPhone = self::formatPhone($psPhone);

		if (preg_match('/^0[3589]{1}00[0-9]{7}$/', $lsPhone) === 1)
		{
			return true;
		}

		return false;
	}

	
	/**
	 * 
	 * @param string $psTelefone
	 * @return string
	 */
	public static function formatarTelefone (string $psTelefone) : string
	{
		$psTelefone = self::formatPhone($psTelefone);
		
		if (strlen($psTelefone) >= 12 && substr($psTelefone, 0, 2) == "55")
		{
			$psTelefone = substr($psTelefone, 2, strlen($psTelefone));
		}
		
		if (substr($psTelefone, 0, 4) == "0800")
		{
			$psTelefone = substr($psTelefone, 0, 4) . "-" . substr($psTelefone, 4, 3) . "-" . substr($psTelefone, 7, 4);
		}
		else if (strlen($psTelefone) == 11)
		{
			$psTelefone = "(" . substr($psTelefone, 0, 2) . ") " . substr($psTelefone, 2, 5) . "-" . substr($psTelefone, 7, 4);
		}		
		else if (strlen($psTelefone) == 10)
		{
			$psTelefone = "(" . substr($psTelefone, 0, 2) . ") " . substr($psTelefone, 2, 4) . "-" . substr($psTelefone, 6, 4);
		}
		
		return $psTelefone;
	}
	
	
	/**
	 * Limpa e adiciona o nono digito caso ainda não tenha
	 *
	 * @param string $psNumber
	 * @param string $psCountryCode
	 * @param string $psCarrier
	 * @return string|null
	 */
	public static function validPhone (string $psNumber, string $psCountryCode = "", string $psCarrier = "") : ?string
	{
		$lsNumber = preg_replace("/[^0-9]/", "", $psNumber);
		
		if (strlen($lsNumber) == 11 || strlen($lsNumber) == 10)
		{
			$lsDDD = substr($lsNumber, 0, 2);
		
			$lsNumber = substr($lsNumber, 2);
			$ls9 = '';
					
			if (strlen($lsNumber) == 8)
			{
				$ls9 = '9';
			}
					
			$lsNumber = $psCountryCode . $psCarrier . $lsDDD . $ls9 . $lsNumber;
			
			return $lsNumber;
		}
			
		return null;
	}
	

	/**
	 * 
	 * @param string|null $psEmail
	 * @return bool
	 */
	public static function validEmail (?string $psEmail) : bool
	{
		$lmResult = preg_match("/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]+)$/", $psEmail);

		return $lmResult > 0 ? true :false;
	}
	

	/**
	 * 
	 * @param string|null $psEmail
	 * @return string
	 */
	public static function maskEmail (?string $psEmail) : string
	{
		$laValue = explode("@", $psEmail);
		$lsValue = "{$laValue[0][0]}***{$laValue[0][-1]}@{$laValue[1]}";

		return $lsValue;
	}

	
	/**
	 * Limpa ou formata CNPJ
	 * 
	 * @param string $psRegistry
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara]
	 * @return string
	 */
	public static function formatCompanyRegistryCNPJ (string $psRegistry, int $pnType = 0) : string
	{
		switch ($pnType)
		{
			case 1:
				$psRegistry = preg_replace("/[^0-9*_]/", "", $psRegistry);
				return preg_replace('/^([0-9]{2})([0-9*]{3})([0-9*]{3})([0-9*]{4})([0-9]{2})$/', '$1.$2.$3/$4-$5', $psRegistry);
				break;
			case 2:
				$psRegistry = preg_replace("/[^0-9*_]/", "", $psRegistry);
				return preg_replace('/^([0-9]{2})([0-9*]{3})([0-9*]{3})([0-9*]{4})([0-9]{2})$/', '$1.***.***/$4-$5', $psRegistry);
				break;				
			default:
				return preg_replace("/[^0-9*_]/", "", $psRegistry);
		}
	}
	
	
	/**
	 * Limpa ou formata CPF
	 * 
	 * @param string|null $psId
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara] 
	 * @return string
	 */
	public static function formatCitizenIdCPF (?string $psId = null, int $pnType = 0) : string
	{
		switch ($pnType)
		{
			case 1:
				$psId = preg_replace("/[^0-9*_]/", "", $psId);
				$psId = str_pad($psId, 11, '0', STR_PAD_LEFT);
				return preg_replace('/^([0-9]{3})([0-9*]{3})([0-9*]{3})([0-9]{2})$/', '$1.$2.$3-$4', $psId);
				break;
			case 2:
				$psId = preg_replace("/[^0-9*_]/", "", $psId);
				$psId = str_pad($psId, 11, '0', STR_PAD_LEFT);
				return preg_replace('/^([0-9]{3})([0-9*]{3})([0-9*]{3})([0-9]{2})$/', '$1.***.***-$4', $psId);
				break;				
			default:
				return preg_replace("/[^0-9*_]/", "", $psId);
		}
	}
	
	
	/**
	 * Limpa ou formata Passaporte
	 * 
	 * @param string|null $psId
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara] 
	 * @return string
	 */
	public static function formatCitizenIdPassaport (?string $psId = null, int $pnType = 0) : string
	{
		switch ($pnType)
		{
			case 1:
				$psId = preg_replace("/[^A-Z0-9*_]/", "", $psId);
				$psId = str_pad($psId, 11, ' ', STR_PAD_RIGHT);
				return preg_replace('/^([A-Z0-9*]{3})([A-Z0-9*]{3})([A-Z0-9]{3})([A-Z]{2})$/', '$1.$2.$3-$4', $psId);
				break;
			case 2:
				$psId = preg_replace("/[^A-Z0-9*_]/", "", $psId);
				$psId = str_pad($psId, 11, ' ', STR_PAD_RIGHT);
				return preg_replace('/^([A-Z0-9*]{3})([A-Z0-9*]{3})([A-Z0-9]{3})([A-Z]{2})$/', '$1.***.***-$4', $psId);
				break;				
			default:
				return preg_replace("/[^A-Z0-9*_]/", "", $psId);
		}
	}


		/**
	 * Limpa ou formata CPF ou Passaporte
	 * 
	 * @param string|null $psId
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara | `3` limpa deixando letra e numero | `4` formatar deixando letra e numero] 
	 * @param bool $pbPassaport
	 * @return string
	 */
	public static function formatCitizenId (?string $psId = null, int $pnType = 0, bool $pbPassaport = false) : string
	{
		if ($pnType == 3)
		{
			return preg_replace("/[^A-Z0-9*_]/", "", $psId);
		}
		elseif ($pnType == 4)
		{
			$psId = preg_replace("/[^A-Z0-9*_]/", "", $psId);
			$psId = str_pad($psId, 11, ' ', STR_PAD_RIGHT);
			return preg_replace('/^([A-Z0-9*]{3})([A-Z0-9*]{3})([A-Z0-9]{3})([A-Z0-9]{2})$/', '$1.$2.$3-$4', $psId);
		}
		
		if ($pbPassaport)
		{
			return self::formatCitizenIdPassaport($psId, $pnType);
		}

		return self::formatCitizenIdCPF($psId, $pnType);
	}


	/**
	 * Limpa ou formata Placa de veículo
	 * 
	 * @param string|null $psId
	 * @param int $pnType [`0` limpa | `1` formata | `2` mascara]
	 * @return string
	 */
	public static function formatVehiclePlate (?string $psId = null, int $pnType = 0) : string
	{
		$psId = strtoupper($psId);

		switch ($pnType)
		{
			case 1:
				return preg_replace('/^([A-Z]{3})([0-9]{1})([A-Z0-9]{1})([0-9]{2})$/', '$1-$2$3$4', $psId);
				break;
			case 2:
				return preg_replace('/^([A-Z]{3})([0-9]{1})([A-Z0-9]{1})([0-9]{2})$/', '$1-**$4', $psId);
				break;				
			default:
				return preg_replace("/[^A-Z]{3}[0-9]{1}[A-Z0-9]{1}[0-9]{2}$/", "", $psId);
		}
	}
	
	
	/**
	 * Valida CPF retornando true ou false
	 * 
	 * @param string|null $psCpf
	 * @param bool $pbZeroFill
	 * @return bool
	 */
	public static function validCPF (?string $psCpf = null, bool $pbZeroFill = true) : bool
	{
		$laCpfFalse = [
			'00000000000' => true,
			'11111111111' => true,
			'22222222222' => true,
			'33333333333' => true,
			'44444444444' => true,
			'55555555555' => true,
			'66666666666' => true,
			'77777777777' => true,
			'88888888888' => true,
			'99999999999' => true,
		];

		// Verifica se um número foi informado
		if (empty($psCpf))
		{
			return false;
		}
	
		// Elimina possivel mascara
		$psCpf = preg_replace('/[^0-9]/', '', $psCpf);

		if ($pbZeroFill)
		{
			$psCpf = str_pad($psCpf, 11, '0', STR_PAD_LEFT);
		}
			
		// Verifica se o numero de digitos informados é igual a 11
		if (strlen($psCpf) != 11)
		{
			return false;
		}

		// Verifica se todos os digitos são iguais
		if (isset($laCpfFalse[$psCpf]))
		{
			return false;
		}

		for ($lnT = 9; $lnT < 11; $lnT++)
		{
			for ($lnD = 0, $lnC = 0; $lnC < $lnT; $lnC++)
			{
				$lnD += $psCpf[$lnC] * (($lnT + 1) - $lnC);
			}

			$lnD = ((10 * $lnD) % 11) % 10;

			if ($psCpf[$lnC] != $lnD)
			{
				return false;
			}
		}
	
		return true;
	}


	/**
	 * Valida CNPJ retornando true ou false
	 * 
	 * @param string|null $psCnpj
	 * @return bool
	 */
	public static function validCNPJ (?string $psCnpj = null) : bool
	{
		$laCnpjFalse = [
			'00000000000000' => true,
			'11111111111111' => true,
			'22222222222222' => true,
			'33333333333333' => true,
			'44444444444444' => true,
			'55555555555555' => true,
			'66666666666666' => true,
			'77777777777777' => true,
			'88888888888888' => true,
			'99999999999999' => true,
		];

		// Verifica se um número foi informado
		if (empty($psCnpj))
		{
			return false;
		}
		
		$psCnpj = preg_replace('/[^0-9]/', '', $psCnpj);

		// Valida tamanho
		if (strlen($psCnpj) != 14)
		{
			return false;
		}

		// Verifica se todos os digitos são iguais
		if (isset($laCnpjFalse[$psCnpj]))
		{
			return false;
		}

		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $lnSoma = 0; $i < 12; $i++) 
		{
			$lnSoma += $psCnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$lnResto = $lnSoma % 11;

		if ($psCnpj[12] != ($lnResto < 2 ? 0 : 11 - $lnResto))
		{
			return false;
		}

		// Valida segundo dígito verificador
		for ($i = 0, $j = 6, $lnSoma = 0; $i < 13; $i++) 
		{
			$lnSoma += $psCnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$lnResto = $lnSoma % 11;

		return $psCnpj[13] == ($lnResto < 2 ? 0 : 11 - $lnResto);
	}
	
	
	/**
	 * Valida CPF retornando true ou false
	 * 
	 * @param string|null $psPassport
	 * @param bool $pbZeroFill
	 * @return bool
	 */
	public static function validPassport (?string $psPassport = null, bool $pbZeroFill = true) : bool
	{
		$laPassportFalse = [
			'000000000' => true,
			'111111111' => true,
			'222222222' => true,
			'333333333' => true,
			'444444444' => true,
			'555555555' => true,
			'666666666' => true,
			'777777777' => true,
			'888888888' => true,
			'999999999' => true,
		];

		// Verifica se um número foi informado
		if (empty($psPassport))
		{
			return false;
		}
	
		// Elimina possivel mascara
		$psPassport = preg_replace('/[^A-Z0-9]/', '', $psPassport);

		if ($pbZeroFill)
		{
			$psPassport = str_pad($psPassport, 11, '0', STR_PAD_LEFT);
		}

		// Verifica se o numero de digitos informados é igual a 11
		if (strlen($psPassport) != 11)
		{
			return false;
		}

		// Verifica se todos os digitos são iguais
		if (isset($laPassportFalse[substr($psPassport, 0, 8)]))
		{
			return false;
		}

		return preg_match('/^[A-Z0-9]{9}[A-Z]{2}$/', $psPassport) ? true : false;
	}


	/**
	 * 
	 * 
	 * @param string $psColor
	 * @return string
	 */
	public static function colorInverse (string $psColor) : string
	{
		$lsColor = str_replace('#', '', $psColor);
			
		if (strlen($lsColor) != 6)
		{ 
			return '000000'; 
		}
			
		$lsRGB = '';
			
		for ($lnX=0; $lnX<3; $lnX++)
		{
			$lnC = 255 - hexdec(substr($lsColor,(2*$lnX),2));
			$lnC = ($lnC < 0) ? 0 : dechex($lnC);
			$lsRGB .= (strlen($lnC) < 2) ? '0'.$lnC : $lnC;
		}

		return "#{$lsRGB}";
	}

	
	/**
	 * 
	 * @param string|null $psIp
	 * @return bool
	 */
	public static function validIp (?string $psIp) : bool
	{
		$lmResult = preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/", $psIp);

		return $lmResult > 0 ? true :false;
	}
}