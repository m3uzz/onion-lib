<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class Date
{

	/**
	 * Retorna um formato de data solicitado
	 *
	 * @param string|null $psDate data no formato `Y/m/d (H|h):i:s` ou `Y-m-d (H|h):i:s`
	 * @param int $pnType indicação de formato de retorno, padrão `0` retornando `d/m/Y - H:i:s`
	 * @return string
	 */
	public static function getDateTimeFormatPtBr (?string $psDate = null, int $pnType = 0) : string
	{
		if (is_null($psDate) || empty($psDate))
		{
			return '';
		}

		$lnDay = (int) substr($psDate, 8, 2);
		$lnMonth = (int) substr($psDate, 5, 2);
		$lnYear = substr($psDate, 0, 4);
		$lsHour = substr($psDate, 11, 8);
		$lsHour2 = substr($psDate, 11, 5);
		$lsHour3 = substr($psDate, 11, 2);

		
		switch ($pnType)
		{
			case 1: // d/m/Y
				return sprintf("%02d/%02d/%s", $lnDay,$lnMonth,$lnYear);
			case 2: // H:i:s
				return $lsHour;
			case 3: // d de M de Y
				$lsMes = self::getMonthNamePtBr($lnMonth);
				return $lnDay . " de " . $lsMes . " de " . $lnYear;
			case 4: // d de m de Y
				$lsMes = self::getMonthNamePtBr($lnMonth, 2);
				return $lnDay . " de " . $lsMes . " de " . $lnYear;
			case 5: // d/m
				return sprintf("%02d/%02d", $lnDay, $lnMonth);
			case 6: // d/m - H:i:s
				return sprintf("%02d/%02d%s", $lnDay, $lnMonth, ($lsHour ? " - " . $lsHour : ""));
			case 7: // H:i
				return substr($lsHour, 0, 5);
			case 8: // Y/m/d - H:i:s
				return sprintf("%s/%02d/%02d%s", $lnYear, $lnMonth, $lnDay, ($lsHour ? " - " . $lsHour : ""));
			case 9: // d/M
				$lsMes = self::getMonthNamePtBr($lnMonth, 1);
				return $lnDay . "/" . $lsMes; 
			case 10: // Y-m-dTH:i:s
				return sprintf("%d-%02d-%02d%s", $lnYear, $lnMonth, $lnDay, ($lsHour ? "T" . $lsHour : ""));
			case 11: // d/M/Y - H:i:s
				return $lnDay . "/" . self::getMonthNamePtBr($lnMonth, 1) . "/" . $lnYear . ($lsHour ? " - " . $lsHour : ""); 
			case 12: // Y-m-d
				return sprintf("%s-%02d-%02d", $lnYear,$lnMonth,$lnDay);
			case 13: // d/m/Y - H:i
				return sprintf("%02d/%02d/%s%s", $lnDay, $lnMonth, $lnYear, ($lsHour2 ? " " . $lsHour2 : ""));
			case 14: // M/Y - H:i
				$lsMes = self::getMonthNamePtBr($lnMonth, 1);
				return $lsMes . "/" . $lnYear;
			case 15: // d/m Hh
				return sprintf("%02d/%02d%s", $lnDay, $lnMonth, ($lsHour3 ? " {$lsHour3}h" : ""));
			case 16: // H:i de d/m/Y
				return sprintf("%s de %02d/%02d/%s", $lsHour2, $lnDay, $lnMonth, $lnYear);
			case 17: // d de m de Y
				$lsMes = self::getMonthNamePtBr($lnMonth, 1);
				return $lnDay . " " . $lsMes . " " . $lnYear;
			default: // d/m/Y - H:i:s
				return sprintf("%02d/%02d/%s%s", $lnDay, $lnMonth, $lnYear, ($lsHour ? " - " . $lsHour : ""));
		}
	}

	
	/**
	 * Retorna o nome do mês em 3 formatos quando passado seu número
	 *
	 * @param int $pnMonth Número do mês no calendário
	 * @param int $pnType Formato: `0` = primeira letra; `1` = Abreviação 3 letras;
	 *        	`2` = padrão nome completo
	 * @return string nome do mês
	 */
	public static function getMonthNamePtBr (int $pnMonth, int $pnType = 2) : ?string
	{
		$laMonths = [
			[
				"J",
				"Jan",
				"Janeiro"
			],
			[
				"F",
				"Fev",
				"Fevereiro"
			],
			[
				"M",
				"Mar",
				"Março"
			],
			[
				"A",
				"Abr",
				"Abril"
			],
			[
				"M",
				"Mai",
				"Maio"
			],
			[
				"J",
				"Jun",
				"Junho"
			],
			[
				"J",
				"Jul",
				"Julho"
			],
			[
				"A",
				"Ago",
				"Agosto"
			],
			[
				"S",
				"Set",
				"Setembro"
			],
			[
				"O",
				"Out",
				"Outubro"
			],
			[
				"N",
				"Nov",
				"Novembro"
			],
			[
				"D",
				"Dez",
				"Dezembro"
			]
		];
		
		return isset($laMonths[$pnMonth - 1][$pnType]) ? $laMonths[$pnMonth - 1][$pnType] : '';
	}

	
	/**
	 *   
	 * @return array
	 */
	public static function getMonthsPtBr () : array
	{
		$laMonths = [
			"1" => "Janeiro",
			"2" => "Fevereiro",
			"3" => "Março",
			"4" => "Abril",
			"5" => "Maio",
			"6" => "Junho",
			"7" => "Julho",
			"8" => "Agosto",
			"9" => "Setembro",
			"10" => "Outubro",
			"11" => "Novembro",
			"12" => "Dezembro"
		];
		
		return $laMonths;
	}

	
	/**
	 * Retorna o nome do dia da semana em 4 formatos quando passado seu número
	 *
	 * @param int $pnDay Número do dia na semana no calendário
	 * @param int $pnType Formato: `0` = primeira letra; `1` = Abreviação 3 letras;
	 *        	`2` = nome simples; `3` = padrão nome completo
	 * @return string nome do mês
	 */
	public static function getWeekNamePtBr (int $pnDay, int $pnType = 3) : string
	{
		$laDays = [
			[
				"D",
				"Dom",
				"Domingo",
				"Domingo"
			],
			[
				"S",
				"Seg",
				"Segunda",
				"Segunda-feira"
			],
			[
				"T",
				"Ter",
				"Terça",
				"Terça-feira"
			],
			[
				"Q",
				"Qua",
				"Quarta",
				"Quarta-feira"
			],
			[
				"Q",
				"Qui",
				"Quinta",
				"Quinta-feira"
			],
			[
				"S",
				"Sex",
				"Sexta",
				"Sexta-feira"
			],
			[
				"S",
				"Sab",
				"Sábado",
				"Sábado"
			]
		];
		
		return $laDays[$pnDay - 1][$pnType];
	}


	/**
	 * 
	 * @param string $psDate
	 * @param string $psFrequency (`y`|`m`|`w`|`d`) default `m`
	 * @param int $pnAdd
	 * @return string
	 */
	public static function nextDate (string $psDate, string $psFrequency = 'm', int $pnAdd = 1) : string
	{
		$lnDate = (int)strtotime($psDate);
		$lnDay = date("d", $lnDate);
		$lnMonth = date("m", $lnDate);
		$lnYear = date("Y", $lnDate);

		switch ($psFrequency)
		{
			case 'y':
				$lnYear += $pnAdd;

				if ($lnMonth == 2 && $lnDay == 29)
				{
					$lnNextDate = strtotime("{$lnYear}-{$lnMonth}-01");
					$lnDay = date('t', $lnNextDate);
				}

				$psNextDate = sprintf("%04d-%02d-%02d", $lnYear, $lnMonth, $lnDay);
				break;
			case 'w':
				$pnNextDate = $lnDate + (60*60*24*7*$pnAdd);
				$psNextDate = date('Y-m-d', $pnNextDate);
				break;
			case 'd':
				$pnNextDate = $lnDate + (60*60*24*$pnAdd);
				$psNextDate = date('Y-m-d', $pnNextDate);
				break;
			default:
				$lnTotalMonths = $lnMonth + $pnAdd;

				if ($lnTotalMonths > 12)
				{
					$lnTotalYears = $lnTotalMonths / 12;
					$lnYear += (int)$lnTotalYears;
					$lnMonth = round(($lnTotalYears - (int)$lnTotalYears) * 12);
				}
				else
				{
					$lnMonth = $lnTotalMonths;
				}

				$lnNextDate = strtotime("{$lnYear}-{$lnMonth}-01");
				$lnNextMonthTotalDays = date('t', $lnNextDate);
				
				if ($lnDay > $lnNextMonthTotalDays)
				{
					$lnDay = $lnNextMonthTotalDays;
				}

				$psNextDate = sprintf("%04d-%02d-%02d", $lnYear, $lnMonth, $lnDay);
		}
		
		return $psNextDate;
	}

	
	/**
	 * 
	 * Calcula e retornar a data de fechamento de um período de ciclo mensal
	 * (dia anterior ao corte até 23:59:59)
	 * Com o dia de início do período ($pnDay), é calculada a data um dia antes,
	 * levando em conta
	 * que os meses tem quantidade de dias diferentes, além de tratar o dia 1 do
	 * mês e do ano. Ou seja, se $pnDay for 1 o dia anterior pode ser 31, 30, 29 ou 28 além do
	 * ano que pode mudar se for 1 de janeiro.
	 *
	 * Ex.: se $pnDay for 10 e o $psDate ou a data atual for 2014-09-20 será
	 * retornado '2014-09-09 23:59:59'
	 *
	 * @param int $pnDay - dia de corte, dia no mês que define o início de um
	 *        	período
	 * @param string|null $psDate - data de referência, se null utiliza a data atual
	 *        	gerada por time()
	 * @return string
	 */
	public static function getPeriodEnd (int $pnDay, ?string $psDate = null) : string
	{
		if ($psDate == null)
		{
			$laToday = explode('-', date('Y-m-d', time()));
		}
		else
		{
			$laToday = explode('-', date('Y-m-d', strtotime($psDate)));
		}
		
		$lnDay = $laToday[2];
		
		if ($pnDay == 1 || $pnDay > $lnDay)
		{
			$lnMonth = ($laToday[1] > 1 ? $laToday[1] - 1 : 12);
			$lnYear = ($lnMonth == 12 ? $laToday[0] - 1 : $laToday[0]);
			$lnLastMonthTotalDays = date('t', strtotime("{$lnYear}-{$lnMonth}-01"));
		}
		else
		{
			$lnMonth = $laToday[1];
			$lnYear = $laToday[0];
			$lnLastMonthTotalDays = date('t', strtotime("{$lnYear}-{$lnMonth}-01"));
		}
		
		$lnDayBefore = $pnDay - 1;
		
		if ($lnDayBefore < 1)
		{
			$lsDate = sprintf('%d-%02d-%02d 23:59:59', $lnYear, $lnMonth, $lnLastMonthTotalDays);
		}
		else
		{
			$lsDate = sprintf('%d-%02d-%02d 23:59:59', $lnYear, $lnMonth, $lnDayBefore);
		}
		
		return $lsDate;
	}

	
	/**
	 * Calcular e retornar a data de início de um período de ciclo mensal
	 * (iniciando as 00:00:00)
	 *
	 * @param string $psDate - data a ser definida o dia do mês anterior
	 * @param int $pnDay
	 * @return string
	 */
	public static function getPeriodStart (string $psDate, int $pnDay) : string
	{
		$lnMonthEnd = date('m', strtotime($psDate));
		$lnMonthTotalDays = date('t', strtotime($psDate));
		$lnDay = date('d', strtotime($psDate));
		
		if ($pnDay == 1 && $lnDay == $lnMonthTotalDays)
		{
			$lnLastMonthTotalDays = $lnMonthTotalDays;
		}
		else
		{
			$laDate = explode("-", $psDate);
			$lnMonth = ($laDate[1] > 1 ? $laDate[1] - 1 : 12);
			$lnYear = ($lnMonth == 12 ? $laDate[0] - 1 : $laDate[0]);
			$lnLastMonthTotalDays = date('t', strtotime("{$lnYear}-{$lnMonth}-01"));
		}
		
		$lnMinus = 1;
		
		if ($lnMonthEnd == 2 && $lnLastMonthTotalDays < 30 && $pnDay == 1)
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 2 && $lnLastMonthTotalDays == 31 && ($pnDay == 28 || $pnDay == 29))
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 2 && $lnLastMonthTotalDays == 31 && $pnDay == 30 && $lnMonthTotalDays == 29)
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 2 && $lnLastMonthTotalDays == 31 && $pnDay == 31 && $lnMonthTotalDays == 28)
		{
			$lnMinus = 2;
		}
		elseif ($lnMonthEnd == 3 && $lnLastMonthTotalDays == 28 && $pnDay == 30)
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 3 && $lnLastMonthTotalDays == 28 && $pnDay == 31)
		{
			$lnMinus = - 1;
		}
		elseif ($lnMonthEnd == 3 && $lnLastMonthTotalDays == 28 && $pnDay < 28)
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 3 && $lnLastMonthTotalDays == 29 && $pnDay == 31)
		{
			$lnMinus = 0;
		}
		elseif ($lnMonthEnd == 3 && $lnLastMonthTotalDays == 29 && $pnDay < 28)
		{
			$lnMinus = 0;
		}
		
		$lsDate = date('Y-m-d 00:00:00', strtotime($psDate) - (($lnLastMonthTotalDays - $lnMinus) * 24 * 60 * 60));
		
		return $lsDate;
	}

	
	/**
	 * 
	 * @param string $psTime
	 * @return int
	 */
	public static function toTime (string $psTime) : int
	{
		$lsToday = date('Y-m-d');
		$lnToday = strtotime($lsToday);
		$lnTime = strtotime("{$lsToday} {$psTime}");
		$lnTime = $lnTime - $lnToday;
		
		return $lnTime;
	}


	/**
	 *
	 * @param int|string|null $pmTime
	 * @param int $pnType
	 * @param bool $pbDay
	 * @return string|array
	 */
	public static function time2Duration ($pmTime = null, int $pnType = 0, bool $pbDay = false)
	{
		$lnDay = 0;

		if ($pbDay)
		{
			$lnDay = (int)($pmTime / 86400);
			$pmTime -= $lnDay * 86400;
		}

		$lnHour = (int)($pmTime / 3600);
		$pmTime -= $lnHour * 3600;

		$lnMinute = (int)($pmTime / 60);
		$pmTime -= $lnMinute * 60;

		$lnSecond = (int)$pmTime;

		$lsDay = ($lnDay > 0 ? "{$lnDay}d ": "");
		$lsHour = ($lnHour > 0 ? "{$lnHour}h ": "");
		$lsMinute = ($lnMinute > 0 ? "{$lnMinute}m ": "");
		$lsSecond = ($lnSecond > 0 ? "{$lnSecond}s ": "");

		switch ($pnType)
		{
			case 1:
				return sprintf("%s%s%s", $lsDay, $lsHour, $lsMinute);
				break;
			case 2:
				return ['d' => $lnDay, 'h' => $lnHour, 'm' => $lnMinute, 's' => $lnSecond];
				break;
			case 3:
				return sprintf("%s%s%s%s", $lsDay, $lsHour, $lsMinute, $lsSecond);
				break;
			case 4:
				return sprintf("%s%d:%02d:%02d", $lsDay, $lnHour, $lnMinute, $lnSecond);
				break;					
			default:
				return sprintf("%s%d:%02d", $lsDay, $lnHour, $lnMinute);
		}
	}
}