<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class Prompt
{
	
	/**
	 *
	 * @param string|null $psMsgError
	 */
	public static function echoError (?string $psMsgError) : void
	{
		echo ("\e[31mERROR - " . $psMsgError . "\e[0m\n\n");
	}
	
	
	/**
	 *
	 * @param string|null $psMsgWarning
	 */
	public static function echoWarning (?string $psMsgWarning) : void
	{
		echo ("\e[33mWARNING - " . $psMsgWarning . "\e[0m\n\n");
	}
	
	
	/**
	 *
	 * @param string|null $psMsgSuccess
	 */
	public static function echoSuccess (?string $psMsgSuccess) : void
	{
		echo ("\e[32mSUCCESS - " . $psMsgSuccess . "\e[0m\n\n");
	}
	
	
	/**
	 *
	 * @param string|null $psMsgInfo
	 */
	public static function echoInfo (?string $psMsgInfo) : void
	{
		echo ("\e[36m" . $psMsgInfo . "\e[0m\n\n");
	}
	
	
	/**
	 *
	 * @param string|null $psMsgInfo
	 * @param string|null $psCommand
	 */
	public static function echoCommand (?string $psMsgInfo, ?string $psCommand) : void
	{
		echo ("\e[36m" . $psMsgInfo . "\e[0m " . $psCommand. "\n\n");
	}
	
	
	/**
	 *
	 * @param string|null $psMsgError
	 */
	public static function exitError (?string $psMsgError) : void
	{
		die (self::echoError($psMsgError));
	}
	
	
	/**
	 *
	 * @param string|null $psMsgWarning
	 */
	public static function exitWarning (?string $psMsgWarning) : void
	{
		die (self::echoWarning($psMsgWarning));
	}
	
	
	/**
	 *
	 * @param string|null $psMsgSuccess
	 */
	public static function exitSuccess (?string $psMsgSuccess) : void
	{
		die (self::echoSuccess($psMsgSuccess));
	}
	
	
	/**
	 *
	 * @param string|null $psMsgInfo
	 */
	public static function exitInfo (?string $psMsgInfo) : void
	{
		die (self::echoInfo($psMsgInfo));
	}
	
	
	/**
	 *
	 * @param string|null $psQuestion
	 * @param string $psDefault
	 * @return string|bool
	 */
	public static function confirm (?string $psQuestion, string $psDefault = 'y')
	{
		$lsDefaultOpt = ($psDefault == 'n' ? 'y/N' : 'Y/n');

		echo "\n\e[36m" . $psQuestion . " [" . $lsDefaultOpt . "]:\e[0m ";

		$lbInteractive = defined('INTERACTIVE') ? constant('INTERACTIVE') : true;
	
		if ($lbInteractive)
		{
			$lsAnswer = null;
			$lrRandle = fopen("php://stdin", "r");
			$lsAnswer = fgets($lrRandle);
			fclose($lrRandle);

			$lsAnswer = strtolower(trim($lsAnswer));
		}
		
		if (empty($lsAnswer))
		{
			$lsAnswer = $psDefault;
		}
		
		switch ($lsAnswer)
		{
			case 'y':
				return true;
				break;
			case 'n':
				return false;
				break;
			default:
				Self::echoInfo("The answer need to be y (yes) or n (no)! Try again.");
				return self::confirm($psQuestion);
		}
	}
	
	
	/**
	 * 
	 * @param string|null $psQuestion
	 * @param string $psDefault
	 * @return string
	 */
	public static function prompt (?string $psQuestion, string $psDefault = "") : string
	{
		echo "\e[34m" . $psQuestion . "\e[0m ";
		
		$lbInteractive = defined('INTERACTIVE') ? constant('INTERACTIVE') : true;
	
		if ($lbInteractive)
		{
			$lrHandle = fopen("php://stdin", "r");
			$lsAnswer = fgets($lrHandle);
			fclose($lrHandle);
		}

		if (empty($lsAnswer))
		{
			$lsAnswer = $psDefault;
		}
		
		return trim($lsAnswer);
	}
}