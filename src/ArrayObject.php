<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class ArrayObject 
{

    /**
     * Merge options recursively
     *
     * @param array $paArray1
     * @param array|null $paArray2
     * @param bool $pbRecursive
	 * @param bool $pbOverwriteNumericKey
     * @return array
     */
    public static function merge (array $paArray1, ?array $paArray2 = null, bool $pbRecursive = false, bool $pbOverwriteNumericKey = true) : array
    {
        if ($pbRecursive)
        {
            if (is_array($paArray2))
            {
                foreach ($paArray2 as $lmKey => $lmVal)
                {
                    if (is_array($paArray2[$lmKey]))
                    {
                        if (array_key_exists($lmKey, $paArray1) && is_array($paArray1[$lmKey]))
                        {
							if (is_int($lmKey))
							{
								$paArray1[] = $paArray2[$lmKey];
							}
							else
							{
                            	$paArray1[$lmKey] = self::merge($paArray1[$lmKey], $paArray2[$lmKey], $pbRecursive);
							}
                        }
                        else 
                        {
                            $paArray1[$lmKey] = $paArray2[$lmKey];
                        }
                    }
                    else
                    {
                        $paArray1[$lmKey] = $lmVal;
                    }
                }
            }
        }
        else 
        {
            $paArray1 = array_merge($paArray1, $paArray2);
        }
        
        return $paArray1;
    }
	
	
	/**
	 * 
	 * @param array $paArray
	 * @param string $psIdentation
	 * @throws \Exception
	 * @return string
	 */
	public static function arrayToString (array $paArray, string $psIdentation = "\t") : string
	{
		if (is_array($paArray))
		{
			$lsString = "";
			
			foreach ($paArray as $lsKey => $lmValue)
			{
				if (is_array($lmValue))
				{
					$lsString .= $psIdentation . "'$lsKey' => [\n" . self::arrayToString($lmValue, $psIdentation . "\t") . $psIdentation ."],\n";
				}
				elseif (is_bool($lmValue))
				{
					$lsString .= $psIdentation . "'$lsKey' => " . ($lmValue ? 'true' : 'false') . ",\n";
				}
				elseif ($lmValue === null)
				{
					$lsString .= $psIdentation . "'$lsKey' => null,\n";
				}
				else 
				{
					if (preg_match("/\/\/array/", (string)$lmValue))
					{
						$lsString .= $psIdentation . "'$lsKey' => " . substr($lmValue, 2) . ",\n";
					}
					else 
					{
						$lsString .= $psIdentation . "'$lsKey' => '$lmValue',\n";
					}
				}
			}
			
			return $lsString;
		}
		else 
		{
			throw new \Exception("The param value should be an array!");
		}
	}
	
	
	/**
	 * 
	 * @param array $paArray
	 * @return string
	 */
	public static function arrayToFile (array $paArray) : string
	{
		return "<?php\nreturn [\n" . self::arrayToString($paArray) . "];";
	}
	
	
	/**
	 * 
	 * @param array $paDados
	 * @return array
	 */
	public static function multArrayUtf8Decode (array $paDados) : array
	{
		if (is_array($paDados))
		{
			foreach ($paDados as $lsCampo => $lsValor)
			{
				$paDados[$lsCampo] = self::multArrayUtf8Decode($lsValor);
			}
		}
		else
		{
			$lsEncode = mb_detect_encoding($paDados . 'x', 'UTF-8, ISO-8859-1');
			
			if ($lsEncode == 'UTF-8')
			{
				$paDados = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $paDados);
			}
			else
			{
				$paDados = iconv("ISO-8859-1", "ISO-8859-1//TRANSLIT", $paDados);
			}
		}
		
		return $paDados;
	}	
	
	
	/**
	 *
	 * @param string $psSessionData
	 * @throws \Exception
	 * @return array
	 */
	public static function unserialize (string $psSessionData) : array
	{
		$lsMethod = ini_get("session.serialize_handler");
		
		switch ($lsMethod)
		{
			case "php":
				return self::unserializePhp($psSessionData);
				break;
			case "php_binary":
				return self::unserializePhpBinary($psSessionData);
				break;
			default:
				throw new \Exception("Unsupported session.serialize_handler: " . $lsMethod . ". Supported: php, php_binary");
		}
	}
	
	
	/**
	 *
	 * @param string $psSessionData
	 * @throws \Exception
	 * @return array
	 */
	public static function unserializePhp (string $psSessionData) : array
	{
		$laReturnData = [];
		$lnOffset = 0;
		
		while ($lnOffset < strlen($psSessionData))
		{
			if (!strstr(substr($psSessionData, $lnOffset), "|"))
			{
				throw new \Exception("invalid data, remaining: " . substr($psSessionData, $lnOffset));
			}
			
			$lnPos = strpos($psSessionData, "|", $lnOffset);
			$lnNum = $lnPos - $lnOffset;
			$lsVarName = substr($psSessionData, $lnOffset, $lnNum);
			$lnOffset += $lnNum + 1;
			$laData = unserialize(substr($psSessionData, $lnOffset));
			$laReturnData[$lsVarName] = $laData;
			$lnOffset += strlen(serialize($laData));
		}
		
		return $laReturnData;
	}
	
	
	/**
	 *
	 * @param string $psSessionData
	 * @return array
	 */
	public static function unserializePhpBinary (string $psSessionData) : array
	{
		$laReturnData = [];
		$lnOffset = 0;
		
		while ($lnOffset < strlen($psSessionData))
		{
			$lnNum = ord($psSessionData[$lnOffset]);
			$lnOffset += 1;
			$lsVarName = substr($psSessionData, $lnOffset, $lnNum);
			$lnOffset += $lnNum;
			$laData = unserialize(substr($psSessionData, $lnOffset));
			$laReturnData[$lsVarName] = $laData;
			$lnOffset += strlen(serialize($laData));
		}
		
		return $laReturnData;
	}
}