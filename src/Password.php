<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use Laminas\Crypt\BlockCipher;
use Laminas\Crypt\Password\Bcrypt;


class Password
{
	
	/**
	 * 
	 * @return string
	 */
	public static function generateDynamicSalt () : string
	{
		$lsDynamicSalt = '';
	
		for ($i = 0; $i < 15; $i ++)
		{
		    $lnAscii = rand(33, 126);
		    
		    if ($lnAscii != 34 && $lnAscii != 39 && $lnAscii != 60 && $lnAscii != 92)
		    {
		        $lsDynamicSalt .= chr($lnAscii);
		    }
		}
	
		return $lsDynamicSalt;
	}	

	
	/**
	 * 
	 * @param string $psPassword
	 * @param string $psDynamicSalt
	 * @param string $psStaticSalt
	 * @return string
	 */
	public static function encriptPassword (string $psPassword, string $psDynamicSalt, string $psStaticSalt="aFGQ475SDsdfsaf2342") : string
	{
	    $psPassword = sha1($psStaticSalt . $psPassword . $psDynamicSalt);
	    
		return $psPassword;
	}

	
	/**
	 * 
	 * @param string $psPassword
	 * @param string $psDynamicSalt
	 * @param string $psStaticSalt
	 * @return string
	 */
	public static function create (string $psPassword, string $psDynamicSalt, string $psStaticSalt="aFGQ475SDsdfsaf2342") : string
	{
		$loBcrypt = new Bcrypt();

		return $loBcrypt->create($psStaticSalt . $psPassword . $psDynamicSalt);
	}


	/**
	 * 
	 * @param string $psPassword
	 * @param string $psSecurePass
	 * @param string $psDynamicSalt
	 * @param string $psStaticSalt
	 * @return bool
	 */
	public static function verify (string $psPassword, string $psSecurePass, string $psDynamicSalt, string $psStaticSalt="aFGQ475SDsdfsaf2342") : bool
	{
		$loBcrypt = new Bcrypt();

		return $loBcrypt->verify($psStaticSalt . $psPassword . $psDynamicSalt, $psSecurePass);
	}

	
	/**
	 * geraSenha: gera uma senha aleatória de 8 digitos, contendo numeros e
	 * letras maiúsculas e minúsculas
	 * 
	 * @param int $pnLenght
	 * @return string
	 */
	public static function generate (int $pnLenght = 8) : string
	{
		// Caracteres de cada tipo
		$lsLower = 'abcdefghijklmnopqrstuvwxyz';
		$lsUpper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$lsNum = '1234567890';
		$lsChar = '!@#%^&*?_~';
		$lsAll = $lsLower . $lsUpper . $lsNum . $lsChar;
		
		$lnRand = mt_rand(1, 26);
		$laReturn[] = $lsLower[$lnRand - 1];
		$lnRand = mt_rand(1, 26);
		$laReturn[] = $lsUpper[$lnRand - 1];
		$lnRand = mt_rand(1, 10);
		$laReturn[] = $lsNum[$lnRand - 1];
		$lnRand = mt_rand(1, 10);
		$laReturn[] = $lsChar[$lnRand - 1];

		for ($ln = 5; $ln <= $pnLenght; $ln ++)
		{
			$lnRand = mt_rand(1, 62);
			$laReturn[] = $lsAll[$lnRand - 1];
		}

		shuffle($laReturn);
		$lsReturn = implode("", $laReturn);
		
		return $lsReturn;
	}


	/**
	 * 
	 * @param string $psPassword
	 * @return int
	 */
	public static function strength (string $psPassword) : int
	{
		$lsUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$lsLowerCase = "abcdefghijklmnopqrstuvwxyz";
		$lsNumber = "0123456789";
		$lsCharacters = "!@#%^&*?_~";
	
		// Reset combination count
		$lnScore = 0;
	
		// Password length
		// -- Less than 4 characters
		if (strlen($psPassword) > 2 && strlen($psPassword) < 5) {
			$lnScore += 5;
		}
		// -- 5 to 7 characters
		else if (strlen($psPassword) > 4 && strlen($psPassword) < 8) {
			$lnScore += 10;
		}
		// -- 8 or more
		else if (strlen($psPassword) > 7) {
			$lnScore += 25;
		}
	
		// Letters
		$lnUpperCount = self::countContain($psPassword, $lsUpperCase);
		$lnLowerCount = self::countContain($psPassword, $lsLowerCase);
		$lnLowerUpperCount = $lnUpperCount + $lnLowerCount;
	
		// -- Letters are all lower case
		if ($lnUpperCount == 0 && $lnLowerCount != 0) { 
			$lnScore += 10; 
		}
		// -- Letters are upper case and lower case
		else if ($lnUpperCount != 0 && $lnLowerCount != 0) { 
			$lnScore += 20; 
		}
	
		// Numbers
		$lnNumberCount = self::countContain($psPassword, $lsNumber);
	
		// -- 1 number
		if ($lnNumberCount == 1) {
			$lnScore += 10;
		}
	
		// -- 3 or more numbers
		if ($lnNumberCount >= 3) {
			$lnScore += 20;
		}
	
		// Characters
		$lnCharacterCount = self::countContain($psPassword, $lsCharacters);
	
		// -- 1 character
		if ($lnCharacterCount == 1) {
			$lnScore += 10;
		}   
	
		// -- More than 1 character
		if ($lnCharacterCount > 1) {
			$lnScore += 25;
		}
	
		// Bonus
		// -- Letters and numbers
		if ($lnNumberCount != 0 && $lnLowerUpperCount != 0) {
			$lnScore += 2;
		}
	
		// -- Letters, numbers, and characters
		if ($lnNumberCount != 0 && $lnLowerUpperCount != 0 && $lnCharacterCount != 0) {
			$lnScore += 3;
		}
	
		// -- Mixed case letters, numbers, and characters
		if ($lnNumberCount != 0 && $lnUpperCount != 0 && $lnLowerCount != 0 && $lnCharacterCount != 0) {
			$lnScore += 5;
		}
	
		return $lnScore;
	}
	
	
	/**
	 * Checks a string for a list of characters
	 * 
	 * @param string $psPassword
	 * @param string $psCheck
	 * @return int
	 */
	private static function countContain (string $psPassword, string $psCheck) : int
	{ 
		// Declare variables
		$lnCount = 0;
		$lnLength = strlen($psPassword);
	
		for ($i = 0; $i < $lnLength; $i++) 
		{
			if (strpos($psCheck, $psPassword[$i]) !== false) 
			{ 
				$lnCount++;
			}
		} 
	
		return $lnCount; 
	}


	/**
	 * 
	 * @param string $psPassword
	 * @param string $psSalt
	 * @return string
	 */
	public static function encrypt (string $psPassword, string $psSalt) : string
	{
		if ($psPassword != null && !empty($psPassword)) 
		{
			$psSalt = substr($psSalt, 0,32);

			$loBlockCipher = BlockCipher::factory('openssl', ['algo' => 'aes']);
			$loBlockCipher->setKey($psSalt);
			$lsEncryptedPass = $loBlockCipher->encrypt($psPassword);

			return $lsEncryptedPass;
		}
		
		return $psPassword;
	}
	
	
	/**
	 * 
	 * @param string $psPassword
	 * @param string $psSalt
	 * @return string|bool
	 */
	public static function decrypt (string $psPassword, string $psSalt)
	{
		if ($psPassword != null && !empty($psPassword)) 
		{
			$psSalt = substr($psSalt, 0,32);
			$loBlockCipher = BlockCipher::factory('openssl', ['algo' => 'aes']);
			$loBlockCipher->setKey($psSalt);
			$lsDecryptedPass = $loBlockCipher->decrypt($psPassword);

			return $lsDecryptedPass;
		}
	
		return $psPassword;
	}	
}