<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;


class Image
{
	/**
	 * @var string
	 */
	protected $sBgColor = '180,180,180';
	
	/**
	 * @var bool
	 */
	protected $bImageStretch = true;
	
	/**
	 * @var bool
	 */
	protected $bBorder = true;
	
	/**
	 * @var string
	 */
	protected $sImageExtension;
	
	/**
	 * @var array
	 */
	protected static $aImageType = [
	        "image/jpeg"=>"jpg",
			"image/pjpeg"=>"jpg",
			"image/png"=>"png",
			"image/gif"=>"gif"
	];
	
	
	/**
	 * 
	 * @param string|null $psBgColor
	 * @param bool|null $pbImageStretch
	 * @param bool|null $pbBorder
	 */
	public function __construct (?string $psBgColor = null, ?bool $pbImageStretch = null, ?bool $pbBorder = null)
	{
		//Verificando se foi setada uma cor para preenchimento de fundo no config		
		if ($psBgColor !== null)
		{
		    $this->sBgColor = $psBgColor;
		}
		elseif (defined('IMAGE_BGCOLOR'))
		{
			$this->sBgColor = constant('IMAGE_BGCOLOR');
		}
		
		//Verificando se foi setada a opção de esticar ou encolher imagem no config
		if ($pbImageStretch !== null)
		{
		    $this->bImageStretch = $pbImageStretch;
		}
		elseif (defined('IMAGE_STRETCH'))
		{
			$this->bImageStretch = constant('IMAGE_STRETCH');
		}
		
		//Verificando se foi setada a opção de esticar ou encolher imagem no config
		if ($pbBorder !== null)
		{
		    $this->bBorder = $pbBorder;
		}
		elseif (defined('IMAGE_BORDER'))
		{
			$this->bBorder = constant('IMAGE_BORDER');
		}
		
		//Debug::debug("Cor de fundo: " . $this->sBgColor);
		//Debug::debug("Esticar: " . $this->bImageStretch);
		//Debug::debug("Borda: " . $this->bBorder);
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psImagePath
	 */
	public function getImageType (string $psImagePath) : string
	{
	    //Extraindo a extensão do nome da imagem
		$this->sImageExtension = substr($psImagePath, -3, 3);
		$this->sImageExtension = strtolower($this->sImageExtension);

		//Debug::debug("Tipo: " . $this->sImageExtension);
				
		return $this->sImageExtension;
	}
	
	
	/**
	 * 
	 * @param string $psImagePath
	 * @return object|bool handle
	 */
	public function getImage (string $psImagePath)
	{
		//Verificando qual o tipo da imagem e utilizando sua função específica de carregamento
		if ($this->sImageExtension == "jpg" || $this->sImageExtension == "peg")
		{	
			$lrImage = ImageCreateFromJpeg($psImagePath);
		}
		elseif ($this->sImageExtension == "gif")
		{
			$lrImage = ImageCreateFromGif($psImagePath);
		}
		
		if ($this->sImageExtension == "png")
		{
			$lrImage = ImageCreateFromPng($psImagePath);
		}

        return $lrImage;
	}
	
	
	/**
	 * 
	 * @param string $psImagePath
	 * @param string $psSavePath
	 * @param int $pnMaxWidth
	 * @param int $pnMaxHeight
	 * @return bool
	 * @throws \Exception
	 */
	public function saveImage (string $psImagePath, string $psSavePath, int $pnMaxWidth, int $pnMaxHeight) : bool
	{
	    $this->getImageType($psImagePath);
	    
	    $lrImage = $this->getImage($psImagePath);

		//Se a imagem tiver sido carregada com sucesso, deve continuar o tratamento
		if (!empty($lrImage))
		{
			//Recuperando as dimensões da imagem
			$lnRealHeight = ImageSY($lrImage);
			$lnRealWidth = ImageSX($lrImage);
			
			if ($pnMaxWidth == "" || $pnMaxWidth == 0)
			{
				$lnMaxWidth = $lnRealWidth;
			}
			else
			{
				$lnMaxWidth = $pnMaxWidth;
			}

			if ($pnMaxHeight == "" || $pnMaxHeight == 0)
			{
				$lnMaxHeight = $lnRealHeight;
			}
			else
			{
				$lnMaxHeight = $pnMaxHeight;
			}
				
			//Criando um handle para tratamento da imagem nas dimensões da mesma
			$lrNewImage = imagecreatetruecolor($lnMaxWidth, $lnMaxHeight);
			
			//Explodindo definições de cor de fundo para pegar separadamente cada camada
			$laBgColor = explode(',', $this->sBgColor);

			//Inicializando novas dimenções da imagem a partir da dimensão atual
			$lnNewHeight = $lnRealHeight;
			$lnNewWidth = $lnRealWidth;

			if ($this->bBorder && !empty($pnMaxHeight) && !empty($pnMaxWidth))
			{
				//Debug::debug("com borda");
				
				//Se a imagem puder ser esticada proporcionalmente
				if ($this->bImageStretch)
				{
					//Se a largura atual ou nova for menor que a largura Máxima ou ideal
					if ($lnNewWidth < $lnMaxWidth)
					{
						//Debug::debug("esticando largura");
						
						//A altura deve aumentar proporcionalmente em relação a largura máxima ou ideal
						$lnNewHeight = $lnMaxWidth * ($lnNewHeight / $lnNewWidth);
						$lnNewWidth = (int)$lnMaxWidth; //Aumenta para a largura máxima
						$lnNewHeight = (int)$lnNewHeight; //Aumenta para a altura máxima
					}
				}
	
				//Se a altura atual ou nova for maior que altura máxima
				if ($lnNewHeight > $lnMaxHeight)
				{
					//Debug::debug("reduzindo altura");
						
					//A largura deve diminuir proporcionalmente em relação a altura máxima ou ideal
					$lnNewWidth = $lnMaxHeight * ($lnNewWidth / $lnNewHeight);
					$lnNewHeight = (int)$lnMaxHeight; //Diminui para a altura máxima
					$lnNewWidth = (int)$lnNewWidth; //Diminui para nova largura
				}
	
				//Se a largura atual ou nova for maior que a largura máxima
				if ($lnNewWidth > $lnMaxWidth)
				{
					//Debug::debug("reduzindo largura");
					
					//A altura deve diminuir proporcionalmente em relação a largura máxima ou ideal
					$lnNewHeight = $lnMaxWidth * ($lnNewHeight / $lnNewWidth);
					$lnNewWidth = (int)$lnMaxWidth; //Diminui para a largura máxima
					$lnNewHeight = (int)$lnNewHeight; //Diminui para a nova altura
				}
			}
			else
			{
				//Debug::debug("sem borda");
				//Debug::debug($lnNewHeight.">".$lnMaxHeight);
				
				//Se a altura atual ou nova for maior que altura máxima
				if ($lnNewHeight > $lnMaxHeight)
				{
					//Debug::debug("reduzindo altura");
						
					//A largura deve diminuir proporcionalmente em relação a altura máxima ou ideal
					$lnNewWidth = $lnMaxHeight * ($lnNewWidth / $lnNewHeight);
					$lnNewHeight = (int)$lnMaxHeight; //Diminui para a altura máxima
					$lnNewWidth = (int)$lnNewWidth; //Diminui para nova largura
				}
				
				//Debug::debug($lnNewWidth.">".$lnMaxWidth);
				
				//Se a largura atual ou nova for maior que a largura máxima
				if ($lnNewWidth > $lnMaxWidth)
				{
					//Debug::debug("reduzindo largura");
						
					//A altura deve diminuir proporcionalmente em relação a largura máxima ou ideal
					$lnNewHeight = $lnMaxWidth * ($lnNewHeight / $lnNewWidth);
					$lnNewWidth = (int)$lnMaxWidth; //Diminui para a largura máxima
					$lnNewHeight = (int)$lnNewHeight; //Diminui para a nova altura
				}

				//Se a imagem puder ser esticada proporcionalmente
				if ($this->bImageStretch)
				{
					//Se a largura atual ou nova for menor que a largura Máxima ou ideal
					if ($lnNewWidth < $lnMaxWidth)
					{
						//Debug::debug("esticando largura");
						
						//A altura deve aumentar proporcionalmente em relação a largura máxima ou ideal
						$lnNewHeight = $lnMaxWidth * ($lnNewHeight / $lnNewWidth);
						$lnNewWidth = (int)$lnMaxWidth; //Aumenta para a largura máxima
						$lnNewHeight = (int)$lnNewHeight; //Aumenta para a altura máxima
					}
					
					//Se a altura atual ou nova for menor que a altura Máxima ou ideal
					if ($lnNewHeight < $lnMaxHeight)
					{
						//Debug::debug("esticando altura");
						
						//A largura deve aumentar proporcionalmente em relação a altura máxima ou ideal
						$lnNewWidth = $lnMaxHeight * ($lnNewWidth / $lnNewHeight);
						$lnNewHeight = (int)$lnMaxHeight; //Aumenta para a altura máxima
						$lnNewWidth = (int)$lnNewWidth; //Aumenta para nova largura
					}
				}
				
			}
			
			//Centralizando imagem dentro do frame
			$lnPosX = ($lnMaxWidth - $lnNewWidth) / 2;
			$lnPosY = ($lnMaxHeight - $lnNewHeight) / 2;
			
			//Debug::debug("Dimensão Original: ".$lnRealWidth."x".$lnRealHeight);
			//Debug::debug("Dimensão Nova: ".$lnNewWidth."x".$lnNewHeight);
			
			if ($this->sImageExtension == "png")
			{				
				//Debug::debug("criando png");
				
				//Ativa a camada de transparência para a imagem 
				imagealphablending($lrNewImage, true);
									
				//Aloca a cor transparente e preenche a nova imagem com isto.
				//Sem isto a imagem vai ficar com fundo preto ao invés de transparente.
				$loTransparent = imagecolorallocatealpha($lrNewImage, 0, 0, 0, 127);
				imagefill($lrNewImage, 0, 0, $loTransparent);					

				//copia o frame na imagem final
				imagecopyresampled($lrNewImage, $lrImage, $lnPosX, $lnPosY, 0, 0, $lnNewWidth, $lnNewHeight, $lnRealWidth, $lnRealHeight);					
				imagealphablending($lrNewImage, false);					

				//Salva a camada alpha (transparência)
				imagesavealpha($lrNewImage, true);				
			}
			elseif ($this->sImageExtension == "gif")
			{
				//Debug::debug("criando gif");
				
				//redimencionando a imagem e criando a imagem final
				imagecopyresized($lrNewImage, $lrImage, $lnPosX, $lnPosY, 0, 0, $lnNewWidth, $lnNewHeight, $lnRealWidth, $lnRealHeight);
			}
			else
			{
				//Debug::debug("criando jpg");
				//definindo a cor de fundo da imagem e preenchendo a camada
				$lrBackground = imagecolorallocate($lrNewImage, (int)$laBgColor[0], (int)$laBgColor[1], (int)$laBgColor[2]);				
				imagefill($lrNewImage, 0, 0, $lrBackground);
				
				//redimencionando a imagem e criando a imagem final
				imagecopyresized($lrNewImage, $lrImage, $lnPosX, $lnPosY, 0, 0, $lnNewWidth, $lnNewHeight, $lnRealWidth, $lnRealHeight);
			}

			$this->renderImage($lrNewImage, $psSavePath);
			
			return true;
		}
		else
		{
			throw new \Exception("Image truncated!");
			return false;
		}
	}
	
	
	/**
	 * 
	 * @param mixed $prImage
	 * @param string $psSavePath
	 * @throws \Exception
	 */
	public function renderImage ($prImage, string $psSavePath) : void
	{
        if ($this->sImageExtension == "jpg" || $this->sImageExtension == "peg")
		{
			if (!imagejpeg($prImage, $psSavePath)) //gravando imagem JPG no destino
			{
			    throw new \Exception("Create image JPG failed!");
			}
		}
		elseif ($this->sImageExtension == "gif")
		{
			if (!imagegif($prImage, $psSavePath)) //gravando imagem GIF no destino
			{
			    throw new \Exception("Create image GIF failed!");
			}
		}
		elseif ($this->sImageExtension == "png")
		{
			if (!imagepng($prImage, $psSavePath)) //gravando imagem PNG no destino
			{
			    throw new \Exception("Create image PNG failed!");
			}
		}
			
		imagedestroy($prImage);	    
	}
}