<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;

defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class System
{
	
	/**
	 *
	 * @return string
	 */
	public static function getStreamData () : string
	{
		$lsData = "";
		
		$lrFp = fopen('php://input', 'r');
		
		if ($lrFp)
		{
			while(!feof($lrFp))
			{
				$lsData .= fgets($lrFp, 1024);
			}
		}
		
		fclose($lrFp);
		
		return $lsData;
	}
	
	
	/**
	 * Metodo de carregamento de arquivo local
	 *
	 * @param string $psLk        	
	 * @return string|bool
	 * @throws \Exception
	 */
	public static function localRequest (string $psFilePath)
	{
		// Garantido a separação de diretorio correta para o SO
		$psFilePath = preg_replace([
			"/\//",
			"/\\\/"
		], DS, $psFilePath);
		
		if (file_exists($psFilePath))
		{
			// Se o arquivo existir no servidor local ele é carregado
			$lsReturn = file_get_contents($psFilePath);
			
			if (! $lsReturn)
			{
				// Caso ocorra algum erro na leitura do arquivo é gerado um log de erro;
			    throw new \Exception("The file {$psFilePath} is not readable! Check file permissions!", 403);
				return false;
			}
		}
		else
		{
			// Caso o arquivo não exista é gerado um log de erro
		    throw new \Exception("The file {$psFilePath} was not found!", 404);
			return false;
		}
		
		// Retornando o conteúdo do arquivo
		return $lsReturn;
	}


	
	/**
	 * 
	 * @param string $psFile
	 * @return array
	 */
	public static function subHash (string $psFile) : array
	{
		$laDirLevel['1'] = substr($psFile, 0, 1);
		$laDirLevel['2'] = substr($psFile, 1, 1);
		$laDirLevel['3'] = substr($psFile, 2, 2);
		
		preg_match("/^([a-z0-9]+)/", $psFile, $laFile);
		
		if (isset($laFile[0]))
		{
			$laDirLevel['4'] = $laFile[0];
		}
		if (isset($laDirLevel['4']))
		{
			$laDirLevel['4'] = substr($laDirLevel['4'], 4);
		}
		
		return $laDirLevel;
	}

	
	/**
	 * 
	 * @param string $psFile
	 * @param int|null $pnChmod
	 * @param string|null $psChown
	 * @param string|null $psChown
	 */
	public static function setCHMOD (string $psFile, ?int $pnChmod = null, ?string $psChown = null, ?string $psChgrp = null) : void
	{
		if ($pnChmod === null && defined('FILE_CHMOD'))
		{
			$pnChmod = constant('FILE_CHMOD');
		}
		
		if ($psChown === null && defined('FILE_CHOWN'))
		{
			$psChown = constant('FILE_CHOWN');
		}
		
		if ($psChgrp === null && defined('FILE_CHGRP'))
		{
			$psChgrp = constant('FILE_CHGRP');
		}
	
		@chmod($psFile, $pnChmod ?? 775);
		@chown($psFile, $psChown ?? 'www-data');
		@chgrp($psFile, $psChgrp ?? 'www-data');
	}

	
	/**
	 * 
	 * @param string $psPath
	 * @param int|null $pnChmod
	 * @param string|null $psChown
	 * @param string|null $psChown
	 */
	public static function createDir (string $psPath, ?int $pnChmod = null, ?string $psChown = null, ?string $psChgrp = null) : void
	{
		if (!file_exists($psPath))
		{
			if (@mkdir($psPath))
			{
				self::setCHMOD($psPath, $pnChmod, $psChown, $psChgrp);
			}
			else
			{
				throw new \Exception("The directory {$psPath} was not created!", 500);
			}
		}
	}
	
	
	/**
	 * createDateDir: parse uma data e devolve o caminho para o arquivo
	 *
	 * @param string $psPath
	 * @param string $psDate
	 * @return string
	 */
	public static function createDateDir (string $psPath, string $psDate) : string
	{
		if (isset($psPath) && isset($psDate))
		{
			$lsDate = preg_replace("/-| |:/", DS, $psDate);
			$psPath .= DS . $lsDate;
			$laPath = explode(DS, $psPath);
			$lsPath = '';

			foreach ($laPath as $lsDir)
			{
				if (!empty($lsDir) || $lsDir == 0)
				{
					$lsPath .= DS . $lsDir;
		
					if (! file_exists($lsPath))
					{
						//Debug::debug("mkdir {$lsPath}");
						mkdir($lsPath);
							
						self::setCHMOD($lsPath);
					}
				}
			}

			return $lsPath;
		}
	}
	
	
	/**
	 * createBalancedDir: parse no arquivo hash e devolve o caminho para o arquivo
	 *
	 * @param string $psPath        	
	 * @param string $psFile        	
	 * @return string
	 */
	public static function createBalancedDir (string $psPath, string $psFile) : string
	{
		if (isset($psPath) && isset($psFile))
		{
			$laHash = self::subHash($psFile);
			$psPath .= DS . implode(DS, $laHash);
			$laPath = explode(DS, $psPath);
			$lsPath = '';

			foreach ($laPath as $lsDir)
			{
				if (!empty($lsDir) || $lsDir == 0)
				{
					$lsPath .= DS . $lsDir;
					
					if (! file_exists($lsPath))
					{
						mkdir($lsPath);
						
						self::setCHMOD($lsPath);
					}
				}
			}

			return $lsPath;
		}
	}

	
	/**
	 * getBalancedDir: parse no arquivo hash e devolve o caminho para o arquivo
	 *     	
	 * @param string $psFile        	
	 * @param string $psPath 
	 * @param bool $pbCheckExist
	 * @return bool|string
	 * @throws \Exception
	 */
	public static function getBalancedDir (string $psFile, string $psPath = "", bool $pbCheckExist = true)
	{
		$laDirLevel = self::subHash($psFile);
		
		$lsPath = $psPath;
		
		$lsRealPath = realpath(constant('CLIENT_DIR') . DS . $lsPath);
		
		foreach ($laDirLevel as $lsDir)
		{
			$lsPath .= DS . $lsDir;
			$lsRealPath .= DS . $lsDir;
			
			if ($pbCheckExist && !file_exists($lsRealPath))
			{
			    throw new \Exception("The directory {$lsRealPath} was not found!", 404);
				return false;
			}
		}
		
		return $lsPath . DS;
	}

	
	/**
	 *
	 * @param string $psFile        	
	 * @param string $psPath
	 * @throws \Exception   	
	 */
	public static function rmBalancedFile (string $psFile, string $psPath) : void
	{
		$lsFileDir = self::getBalancedDir($psFile, $psPath);
		$lsFileDir = realpath(constant('CLIENT_DIR') . DS . $lsFileDir);
		
		if (file_exists($lsFileDir . DS . $psFile))
		{
			//Debug::debug('rm ' . $lsFileDir . DS . $psFile);
			self::removeFile($lsFileDir . DS . $psFile);
		}
		else
		{
		    throw new \Exception("The file {$lsFileDir}{$psFile} was not found!", 404);
			//Debug::debug($lsFileDir . $psFile . ' not found.');
		}
	}

	
	/**
	 * Shell command
	 *
	 * @param string $psCommand	string a ser executada no sistema
	 * @param string $psCommand2        	
	 * @return array
	 * @throws \Exception
	 */
	public static function execute (string $psCommand, string $psCommand2 = "") : array
	{
		$laOutput = [];
		$lnReturn = 0;
		
		$lsReturn = exec($psCommand . " 2>&1 " . $psCommand2, $laOutput, $lnReturn);
		
		if ($lnReturn != 0)
		{
		    //throw new \Exception($lsReturn, $lnReturn);
		}
		
		return $laOutput;
	}


	/**
	 * Command line execute in background (nohup)
	 *
	 * @param string $psCommand	string a ser executada no sistema
	 * @return resource|false
	 */
	public static function backExec (string $psCommand)
	{
		if (!empty($psCommand))
		{
			Debug::debug($psCommand);
			$laPipes = [];
			$lrReturn = proc_open("nohup {$psCommand} >/dev/null 2>&1 &", [["pipe", "r"],["pipe", "w"],["pipe", "a"]], $laPipes);
			proc_close($lrReturn);
			Debug::debug($laPipes);
			Debug::debug($lrReturn);

			return $lrReturn;
		}
		
		return false;
	}

	
	/**
	 *
	 * @param string $psTempName        	
	 * @param string $psDestiny 	
	 * @return bool
	 * @throws \Exception
	 */
	public static function moveUploadedFile (string $psTempName, string $psDestiny) : bool
	{
		$lbOk = true;
		
		$laDir = explode("/", $psDestiny);
		
		if (empty($laDir[0]))
		{
			unset($laDir[0]);
			$laDir[1] = "/" . $laDir[1];
		}
		
		$lnCountArray = count($laDir);
		$lsFile = $laDir[$lnCountArray - 1];
		unset($laDir[$lnCountArray - 1]);
		
		$lsDirx = "";

		foreach ($laDir as $lsDir)
		{
			$lsDirx .= $lsDir;
			
			if (strcmp($lsDirx, "..") != 0)
			{
				if (! is_dir($lsDirx))
				{
					mkdir($lsDirx);
				}
			}
			
			$lsDirx .= "/";
		}
		
		if (! move_uploaded_file($psTempName, $lsDirx . $lsFile))
		{
		    throw new \Exception("The file {$psTempName} can't be moved to {$lsDirx}{$lsFile}!", 403);
			$lbOk = false;
		}
		else
		{
			self::setCHMOD($lsDirx . $lsFile);
		}
		
		return $lbOk;
	}

	
	/**
	 * Cria link simbolico no sistema
	 *
	 * @param string $psSource        	
	 * @param string $psLink        	
	 * @return void
	 * @throws \Exception
	 */
	public static function simblink (string $psSource, string $psLink) : void
	{
		if (file_exists($psSource))
		{
			//Debug::debug("ln -s {$psSource} {$psLink}");
			self::execute("ln -s {$psSource} {$psLink}");
		}
		else
		{
		    throw new \Exception("The file {$psSource} was not found!", 404);
			//Debug::debug($psSource . ' not found.');
		}
	}

	
	/**
	 * Create or update a new file in system
	 *
	 * @param string $psFileName path and file name
	 * @param string $psContent
	 * @param string $psMod file save mod (APPEND, NEW)
	 * @return bool
	 * @throws \Exception
	 */
	public static function saveFile (string $psFileName, string $psContent, string $psMod = "NEW") : bool
	{
		$lnMod = 0;
		
		if ($psMod === "APPEND")
		{
			$lnMod = defined('FILE_APPEND') ? constant('FILE_APPEND') : 0;
		}
		
		if (@file_put_contents($psFileName, $psContent, $lnMod))
		{
			self::setCHMOD($psFileName);
		}
		else 
		{
			throw new \Exception("Failed when tring to write in {$psFileName}!", 403);
			return false;
		}
		
		return true;
	}

	
	/**
	 *
	 * @param string $psFileNome path and file name to remove
	 * @return bool
	 * @throws \Exception
	 */
	public static function removeFile (string $psFileName) : bool
	{
		if (is_file($psFileName))
		{
			if (!unlink($psFileName))
			{
				throw new \Exception("Failed when tring to remove file {$psFileName} from the system!", 403);
			}
		}
		else
		{
			throw new \Exception("The file {$psFileName} was not found!", 404);
			//Debug::debug($psFileNome . ' not found.');
		}
		
		return true;
	}

	
	/**
	 *
	 * @param string $psFileNome path and file name to remove
	 * @return bool
	 * @throws \Exception
	 */
	public static function removeSimblink (string $psFileName) : bool
	{
		if (is_link($psFileName))
		{
			if (!unlink($psFileName))
			{
				throw new \Exception("Failed when tring to remove file {$psFileName} from the system!", 403);
			}
		}
		else
		{
			throw new \Exception("The file {$psFileName} was not found!", 404);
			//Debug::debug($psFileNome . ' not found.');
		}
		
		return true;
	}
	
	
	/**
	 *
	 * @param string $psPath
	 * @throws \Exception
	 */
	public static function removeDir (string $psPath) : void
	{
		if (is_dir($psPath))
		{
			//Debug::debug('rm -rf ' . $psPath);
			self::execute("rm -rf $psPath");
		}
		else
		{
		    throw new \Exception("The directory {$psPath} was not found!", 404);
			//Debug::debug($psPath . ' not found.');
		}
	}
	

	/**
	 * 
	 * @param string $psPathDir
	 * @return bool|null
	 */
	public static function isEmptyDir (string $psPathDir)
	{
		if (!empty($psPathDir))
		{
			$loDir = dir($psPathDir);

			while (false !== ($lsEntry = $loDir->read())) 
			{
				if ($lsEntry !== '.' && $lsEntry !== '..')
				{
					$loDir->close();
					return false;
				}
			}

			$loDir->close();

			return true;
		}

		return null;
	}


	/**
	 * 
	 * @param $psBasePath
	 * @param $psPathToRemove
	 * @param $pbJustEmpty
	 * @return bool
	 */
	public static function removeRecursiveDir (string $psBasePath, string $psPathToRemove, bool $pbJustEmpty = true) : bool
	{
		$psPathToRemove = trim($psPathToRemove, " " . DS);
		$laDir = explode(DS, $psPathToRemove);

		for ($lnKey = count($laDir)-1; $lnKey >= 0; $lnKey--)
		{
			//Debug::display($lnKey);
			//Debug::display($laDir[$lnKey]);
			$lsRealPath = $psBasePath . DS . $psPathToRemove;
			//Debug::display($lsRealPath);

			if (isset($laDir[$lnKey]) && !empty($laDir[$lnKey]))
			{
				if ($pbJustEmpty)
				{
					if (self::isEmptyDir($lsRealPath) === true)
					{
						//Debug::display('vazio');
						self::removeDir($lsRealPath);
					}
					else
					{
						//Debug::display('cheio');
						return false;
					}
				}
				else
				{
					//Debug::display('tanto faz');
					self::removeDir($lsRealPath);
				}
			}

			unset($laDir[$lnKey]);
			$psPathToRemove = implode(DS, $laDir);
		}

		return true;
	}

	
	/**
	 *
	 * @param string $psFile
	 * @throws \Exception      	
	 */
	public static function compact (string $psFile) : void
	{
		if (file_exists($psFile))
		{
			//Debug::debug('gzip -f ' . $psFile);
			self::execute("gzip -f $psFile");
		}
		else
		{
		    throw new \Exception("The file {$psFile} was not found!", 404);
			//Debug::debug($psFile . ' not found.');
		}
	}
	
	
	/**
	 * 
	 * @param string $psPath
	 * @throws \Exception
	 */
	public static function tar (string $psPath) : void
	{
		if (file_exists($psPath))
		{
			$lsPath = strstr("/", $psPath, true);
			$lsDir = strstr("/", $psPath);
			//Debug::debug('tar -cf ' . $psPath);
			self::execute("cd {$lsPath}; tar -cf {$psPath}.tar .{$lsDir}");
		}
		else
		{
		    throw new \Exception("The file or directory {$psPath} was not found!", 404);
			//Debug::debug($psPath . ' not found.');
		}
	}
	
	
	/**
	 *
	 * @param string $psPath
	 * @param string $psName
	 * @throws \Exception
	 */
	public static function tarGz (string $psPath, string $psDir) : void
	{
		if (file_exists($psPath))
		{
			//Debug::debug("cd {$psPath}; tar -czf {$psDir}.tar.gz {$psDir}");
			self::execute("cd {$psPath}; tar -czf {$psDir}.tar.gz {$psDir}");
		}
		else
		{
		    throw new \Exception("The file or directory {$psPath} was not found!", 404);
			//Debug::debug($psPath . ' not found.');
		}
	}


	/**
	 * 
	 * @param string $psPath
	 * @return int|null
	 */
	public static function diskUsage (string $psPath) : ?int
	{
		$lrResource = popen('/usr/bin/du -sb ' . $psPath, 'r');
		$lsLine = fgets($lrResource, 4096);
		$lsSize = substr($lsLine, 0, strpos($lsLine, "\t"));
		pclose($lrResource);

		return (int)$lsSize;
	}
}