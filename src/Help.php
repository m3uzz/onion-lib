<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;
use OnionLib\Str;


defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class Help
{
	const VERSION	= '3.17-08';
	
	const BLACK 	= "30m";
	const RED 		= "31m";
	const GREEN 	= "32m";
	const BROWN 	= "33m";
	const BLUE 		= "34m";
	const PURPLE	= "35m";
	const CYAN 		= "36m";
	const GRAY 		= "37m"; 
	
	const BGBLACK 	= "\e[40m";
	const BGRED 	= "\e[41m";
	const BGGREEN 	= "\e[42m";
	const BGYELLOW 	= "\e[43m";
	const BGBLUE 	= "\e[44m";
	const BGPURPLE 	= "\e[45m";
	const BGCYAN 	= "\e[46m";
	const BGWHITE 	= "\e[47m";
	
	const N 		= "0;"; //normal
	const B 		= "1;"; //bold
	const I 		= "3;"; //italic
	const S 		= "4;"; //sublinhado
	
	const CLOSE		= "\e[0m\n\n";
	
	private $aList = [];
	private $aHelp = [];
	private $aModuleHelp = null;
	private $sLastTopic = "";
	
	
	/**
	 * 
	 */
	public function __construct ()
	{
		$this->set("        *** Help - Version: " . self::VERSION . " ***        ", self::PURPLE, self::BGBLACK);
		$this->set("\n");
		$this->set("AUTHOR:  Humberto Lourenço <betto@m3uzz.com>             ", self::CYAN, self::BGBLACK);
		$this->set("\n");
		$this->set("LINK:    http://github.com/m3uzz/onion-lib               ", self::CYAN, self::BGBLACK);
		$this->set("\n");
		$this->set("LICENCE: http://www.opensource.org/licenses/BSD-3-Clause ", self::CYAN, self::BGBLACK);
		$this->set("\n\n");
		
		return $this;
	}
	
	
	/**
	 * 
	 */
	public function clear () : void
	{
		$this->aList = [];
		$this->aHelp = [];
		$this->aModuleHelp = null;
		$this->sLastTopic = "";
	}
	
	
	/**
	 *
	 * @param string $psModule
	 * @return array
	 */
	public function getModuleHelp (string $psModule) : array
	{
		$psModule = Str::lcfirst($psModule);
	
		if (isset($this->aModuleHelp[$psModule]))
		{
			$laModule[$psModule] = $this->aModuleHelp[$psModule];
			
			return $laModule;
		}
		
		return [];
	}
	
	
	/**
	 *
	 * @param string $psModule
	 * @param string $psController
	 * @return array
	 */
	public function getControllerHelp (string $psModule, string $psController) : array
	{
		$psModule = Str::lcfirst($psModule);
		$psController = Str::lcfirst($psController);
	
		if (isset($this->aModuleHelp[$psModule][$psController]))
		{
			$laModule[$psModule][$psController] = $this->aModuleHelp[$psModule][$psController];
				
			return $laModule;
		}
	
		return [];
	}
	
	
	/**
	 *
	 * @param string $psModule
	 * @param string $psController
	 * @param string $psAction
	 * @return array
	 */
	public function getActionHelp (string $psModule, string $psController, string $psAction) : array
	{
		$psModule = Str::lcfirst($psModule);
		$psController = Str::lcfirst($psController);
	
		if (isset($this->aModuleHelp[$psModule][$psController][$psAction]))
		{
			$laModule[$psModule][$psController][$psAction] = $this->aModuleHelp[$psModule][$psController][$psAction];
			
			return $laModule;
		}
	
		return [];
	}
	
	
	/**
	 * 
	 * @param string $psModule
	 * @param string $psController
	 * @param string $psAction
	 * @param string $psParam
	 * @return string|null
	 */
	public function getParamHelp (string $psModule, string $psController, string $psAction, string $psParam) : ?string
	{
		$psModule = Str::lcfirst($psModule);
		$psController = Str::lcfirst($psController);
		
		if (isset($this->aModuleHelp[$psModule][$psController][$psAction]["params"][$psParam]))
		{
			return $this->aModuleHelp[$psModule][$psController][$psAction]["params"][$psParam];
		}
		
		return null;
	}
	
	
	/**
	 * 
	 */
	public function prepare () : void
	{
		if (is_array($this->aHelp))
		{
			foreach ($this->aHelp as $lsTopic => $laLine)
			{
				$this->set("\n\n" . $lsTopic . ":\n", self::BROWN, "", self::B);
				
				if (is_array($laLine))
				{
					foreach ($laLine as $lsLine => $lsDescription)
					{
						$this->set("  " . str_pad($lsLine, 30), self::GREEN);
						$this->set($lsDescription . "\n");
					}
				}
			}
		}
		
		$this->renderModuleHelp();
	}


	/**
	 * 
	 * @param bool $pbPrepare
	 * @return string
	 */
	public function display (bool $pbPrepare = true) : string
	{
		if ($pbPrepare)
		{
			$this->prepare();
		}

		$lsResponse = '';
		
		if (is_array($this->aList))
		{
			foreach ($this->aList as $lsLine)
			{
				$lsResponse .= $lsLine;
			}
		}
		
		$lsResponse .= self::CLOSE;
		
		return $lsResponse;
	}


	/**
	 * 
	 * @return string
	 */
	public function __toString()
	{
		return $this->display();
	}

	
	/**
	 * 
	 * @param string $psTxt
	 * @param string $psColor
	 * @param string $psBg
	 * @param string $psStyle
	 */
	public function set (string $psTxt, string $psColor = self::GRAY, string $psBg = "", string $psStyle = self::N) : void
	{
		$this->aList[] = "\e[" . $psStyle . $psColor . $psBg . $psTxt . "\e[0m";
	}
	

	/**
	 * 
	 * @param string $psTopic
	 */
	public function setTopic (string $psTopic) : Help
	{
		$this->aHelp[$psTopic] = [];
		$this->sLastTopic = $psTopic;
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psLine
	 * @param string $psDescription
	 * @param string|null $psTopic
	 */
	public function setLine (string $psLine, string $psDescription = "", ?string $psTopic = null) : Help
	{
		if ($psTopic == null)
		{
			$psTopic = $this->sLastTopic;
		}
		
		$this->aHelp[$psTopic][$psLine] = $psDescription;
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param array $laModule
	 */
	public function setModuleHelp (array $laModule) : Help
	{
		$this->aModuleHelp = $laModule;

		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psHelpPath
	 */
	public function factory (string $psHelpPath) : Help
	{
		$lsHelpPath = $psHelpPath . DS . "help.php";
		
		if (file_exists($lsHelpPath))
		{
			$this->aModuleHelp = include ($lsHelpPath);
		}

		return $this;
	}

	
	/**
	 * 
	 */
	public function renderModuleHelp () : Help
	{
		if (is_array($this->aModuleHelp))
		{
			foreach ($this->aModuleHelp as $lsModule => $laControllerHelp)
			{
				$this->set("\n\nModule: -m=" . $lsModule . "\n", self::BROWN, "", self::B);
				
				if (is_array($laControllerHelp))
				{
					foreach ($laControllerHelp as $lsController => $laActionsHelp)
					{
						$this->set(" - Controller: -c=" . $lsController . "\n", self::BROWN);
						
						if (is_array($laActionsHelp))
						{
							foreach ($laActionsHelp as $lsAction => $laAction)
							{
								$this->set("  - Action: -a=" . $lsAction, self::CYAN);
									
								if (isset($laAction['desc']) && !empty($laAction['desc']))
								{
									$this->set(" - " . $laAction['desc'], self::GRAY, "", self::I);
								}
								
								$this->set("\n");
								
								if (isset($laAction['params']) && is_array($laAction['params']) && count($laAction['params']) > 0)
								{
									foreach ($laAction['params'] as $lsParam => $lsParamDesc)
									{
										$this->set("    " . str_pad($lsParam, 28), self::GREEN);
										$this->set($lsParamDesc . "\n");
									}
								}
								else 
								{
									$this->set("    No param request\n", self::BLACK);
								}
								
								$this->set("\n");
							}
						}
					}
				}
			}
		}
		
		return $this;
	}
}