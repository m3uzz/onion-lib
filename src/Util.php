<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLib
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-lib
 */
declare (strict_types = 1);

namespace OnionLib;

class Util
{
	/**
	 *
	 * @param string &$psSubject
	 * @param string $psPattern        	
	 * @param string $psReplace        	
	 * @param string $psMod        	
	 */
	public static function parse (string &$psSubject, string $psPattern, ?string $psReplace, string $psMod = "") : void
	{
		$lsP = "/" . $psPattern . "/" . $psMod;
		$psSubject = preg_replace($lsP, (string)$psReplace, $psSubject);
	}

	
	/**
	 *
	 * @param string $psCookie_name        	
	 * @param string $psCookie_value        	
	 * @param int $pnTempo        	
	 */
	public static function storeCookie (string $psCookie_name, string $psCookie_value, int $pnTempo) : void
	{
		setcookie("{$psCookie_name}", "{$psCookie_value}", $pnTempo, "{$_SERVER['SCRIPT_FILENAME']}", "{$_SERVER['HTTP_HOST']}");
	}

	
	/**
	 *
	 * @return float
	 */
	public static function getMicrotime () : float
	{
		list ($lnUsec, $lnSec) = explode(" ", microtime());
		return ((float) $lnUsec + (float) $lnSec);
	}

	
	/**
	 * Setar data no formato para inserção no BD
	 *
	 * @param string $psData
	 * @return string
	 */
	public static function setDateBD (string $psData) : string
	{
		$lsData = implode("-", array_reverse(explode("/", $psData)));
		return $lsData;
	}

	
	/**
	 * Verifica se o valor de uma variável é true ou false e retorna boolo
	 *
	 * @param string|int|bool $pmVar        	
	 * @return bool
	 */
	public static function toBoolean ($pmVar) : bool
	{
		if (is_null($pmVar) || $pmVar === "0" || $pmVar === 0 || $pmVar === false || $pmVar === "false" || $pmVar === "")
		{
			return false;
		}
		else
		{
			return true;
		}
	}


	/**
	 * Corrige problema de somar/subtrair valores com pontos flutuantes
	 * 
	 * @param float|string $pmValue1
	 * @param float|string $pmValue2
	 * @param int $pnDecimals
	 * @return float
	 */
	public static function sumFloat ($pmValue1, $pmValue2, int $pnDecimals = 3) : float
	{
		return self::calcFloat($pmValue1, $pmValue2, $pnDecimals);
	}


	/**
	 * Corrige problema de somar/subtrair valores com pontos flutuantes
	 * 
	 * @param float|string $pmValue1
	 * @param float|string $pmValue2
	 * @param int $pnDecimals
	 * @return float
	 */
	public static function subFloat ($pmValue1, $pmValue2, int $pnDecimals = 3) : float
	{
		return self::calcFloat($pmValue1, $pmValue2, $pnDecimals, false);
	}


	/**
	 * Corrige problema de somar/subtrair valores com pontos flutuantes
	 * 
	 * @param float|string $pmValue1
	 * @param float|string $pmValue2
	 * @param int $pnDecimals
	 * @param bool $pbSum
	 * @return float
	 */
	public static function calcFloat ($pmValue1, $pmValue2, int $pnDecimals = 3, bool $pbSum = true) : float
	{
		if ($pbSum)
		{
			$lnResult = ($pmValue1 + $pmValue2);
		}
		else
		{
			$lnResult = ($pmValue1 - $pmValue2);
		}

		if (is_float($lnResult))
		{
			$lnResult = round($lnResult, $pnDecimals);
		}

		return $lnResult;
	}	


	/**
	 * 
	 * @param array $paRange - ["m" => 15, "f" => 15, "a" => 30]
	 * @param string $psGender - m or f
	 * @param string $psPath - photo
	 * @return string
	 */
	public static function fakePhoto (array $paRange = ["m" => 17, "f" => 17, "a" => 34], string $psGender = '', string $psPath = 'public/photo') : string
	{
		$laRange = ["m" => 17, "f" => 17, "a" => 34];
		$laRange = array_merge($laRange, $paRange);
		
		switch ($psGender)
		{
			case 'M':
			case 'm':
				$psPhoto = rand(1, (int)$laRange["m"]);
				return DS . $psPath . DS . 'm' . DS . $psPhoto . '.jpg';
			case 'F':
			case 'f':
				$psPhoto = rand(1, (int)$laRange["f"]);
				return DS . $psPath . DS . 'f' . DS . $psPhoto . '.jpg';
			default:
				$psPhoto = rand(1, (int)$laRange["a"]);
				return DS . $psPath . DS . $psPhoto . '.jpg';
		}
	}
}